﻿namespace CatanBoardMaker
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.labelLeft = new System.Windows.Forms.Label();
            this.token11 = new System.Windows.Forms.NumericUpDown();
            this.label24 = new System.Windows.Forms.Label();
            this.token12 = new System.Windows.Forms.NumericUpDown();
            this.label25 = new System.Windows.Forms.Label();
            this.token10 = new System.Windows.Forms.NumericUpDown();
            this.label26 = new System.Windows.Forms.Label();
            this.token9 = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.token8 = new System.Windows.Forms.NumericUpDown();
            this.label21 = new System.Windows.Forms.Label();
            this.token6 = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.token5 = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.token3 = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.token4 = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.token2 = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.radioButtonRandom = new System.Windows.Forms.RadioButton();
            this.radioButtonGold = new System.Windows.Forms.RadioButton();
            this.radioButtonSpice = new System.Windows.Forms.RadioButton();
            this.radioButtonFish = new System.Windows.Forms.RadioButton();
            this.radioButtonCouncil = new System.Windows.Forms.RadioButton();
            this.radioButtonDesert = new System.Windows.Forms.RadioButton();
            this.radioButtonForest = new System.Windows.Forms.RadioButton();
            this.radioButtonHills = new System.Windows.Forms.RadioButton();
            this.radioButtonMountians = new System.Windows.Forms.RadioButton();
            this.radioButtonPasture = new System.Windows.Forms.RadioButton();
            this.radioButtonFields = new System.Windows.Forms.RadioButton();
            this.label13 = new System.Windows.Forms.Label();
            this.GoldCount = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.SpiceCount = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBoxBeachTiles = new System.Windows.Forms.CheckBox();
            this.radioButtonOrangeSun = new System.Windows.Forms.RadioButton();
            this.radioButtonGreenMoon = new System.Windows.Forms.RadioButton();
            this.radioButtonSea = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.FishCount = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.CouncilCount = new System.Windows.Forms.NumericUpDown();
            this.DesertCount = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ForestCount = new System.Windows.Forms.NumericUpDown();
            this.HillsCount = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.MountainsCount = new System.Windows.Forms.NumericUpDown();
            this.PastureCount = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.FieldsCount = new System.Windows.Forms.NumericUpDown();
            this.OrangeSunCount = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.GreenMoonCount = new System.Windows.Forms.NumericUpDown();
            this.SeaCount = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.token11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.token12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.token10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.token9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.token8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.token6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.token5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.token3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.token4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.token2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GoldCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpiceCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FishCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CouncilCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DesertCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ForestCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HillsCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MountainsCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PastureCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FieldsCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrangeSunCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GreenMoonCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SeaCount)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(220, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(254, 605);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            this.pictureBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.41176F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.29412F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.29412F));
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(739, 611);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.Location = new System.Drawing.Point(480, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(256, 605);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox2_Paint);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.labelLeft);
            this.panel1.Controls.Add(this.token11);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.token12);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.token10);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.token9);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.token8);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.token6);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.token5);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.token3);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.token4);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.token2);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.radioButtonRandom);
            this.panel1.Controls.Add(this.radioButtonGold);
            this.panel1.Controls.Add(this.radioButtonSpice);
            this.panel1.Controls.Add(this.radioButtonFish);
            this.panel1.Controls.Add(this.radioButtonCouncil);
            this.panel1.Controls.Add(this.radioButtonDesert);
            this.panel1.Controls.Add(this.radioButtonForest);
            this.panel1.Controls.Add(this.radioButtonHills);
            this.panel1.Controls.Add(this.radioButtonMountians);
            this.panel1.Controls.Add(this.radioButtonPasture);
            this.panel1.Controls.Add(this.radioButtonFields);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.GoldCount);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.SpiceCount);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.checkBoxBeachTiles);
            this.panel1.Controls.Add(this.radioButtonOrangeSun);
            this.panel1.Controls.Add(this.radioButtonGreenMoon);
            this.panel1.Controls.Add(this.radioButtonSea);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.FishCount);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.CouncilCount);
            this.panel1.Controls.Add(this.DesertCount);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.ForestCount);
            this.panel1.Controls.Add(this.HillsCount);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.MountainsCount);
            this.panel1.Controls.Add(this.PastureCount);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.FieldsCount);
            this.panel1.Controls.Add(this.OrangeSunCount);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.GreenMoonCount);
            this.panel1.Controls.Add(this.SeaCount);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(211, 605);
            this.panel1.TabIndex = 3;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Base [19]",
            "Base Extension [30]",
            "[102]",
            "E&P 1 [51]",
            "E&P 2 [58]",
            "E&P 3 [58]",
            "E&P 4 [65]",
            "E&P 5 [72]",
            "E&P 2 Extension [79]",
            "E&P 3 Extension [88]",
            "E&P 4 Extension [88]",
            "E&P 5 Extension [97]",
            "[48]",
            "[37]"});
            this.comboBox1.Location = new System.Drawing.Point(3, 581);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(205, 21);
            this.comboBox1.TabIndex = 76;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // labelLeft
            // 
            this.labelLeft.AutoSize = true;
            this.labelLeft.Location = new System.Drawing.Point(4, 546);
            this.labelLeft.Name = "labelLeft";
            this.labelLeft.Size = new System.Drawing.Size(49, 13);
            this.labelLeft.TabIndex = 75;
            this.labelLeft.Text = "Leftover:";
            // 
            // token11
            // 
            this.token11.Location = new System.Drawing.Point(135, 471);
            this.token11.Name = "token11";
            this.token11.Size = new System.Drawing.Size(32, 20);
            this.token11.TabIndex = 74;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(114, 473);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(22, 13);
            this.label24.TabIndex = 73;
            this.label24.Text = "11:";
            // 
            // token12
            // 
            this.token12.Location = new System.Drawing.Point(135, 497);
            this.token12.Name = "token12";
            this.token12.Size = new System.Drawing.Size(32, 20);
            this.token12.TabIndex = 72;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(114, 499);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(22, 13);
            this.label25.TabIndex = 71;
            this.label25.Text = "12:";
            // 
            // token10
            // 
            this.token10.Location = new System.Drawing.Point(135, 445);
            this.token10.Name = "token10";
            this.token10.Size = new System.Drawing.Size(32, 20);
            this.token10.TabIndex = 70;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(114, 447);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(22, 13);
            this.label26.TabIndex = 69;
            this.label26.Text = "10:";
            // 
            // token9
            // 
            this.token9.Location = new System.Drawing.Point(75, 497);
            this.token9.Name = "token9";
            this.token9.Size = new System.Drawing.Size(32, 20);
            this.token9.TabIndex = 68;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(61, 499);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(16, 13);
            this.label19.TabIndex = 67;
            this.label19.Text = "9:";
            // 
            // token8
            // 
            this.token8.Location = new System.Drawing.Point(76, 471);
            this.token8.Name = "token8";
            this.token8.Size = new System.Drawing.Size(32, 20);
            this.token8.TabIndex = 64;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(62, 473);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(16, 13);
            this.label21.TabIndex = 63;
            this.label21.Text = "8:";
            // 
            // token6
            // 
            this.token6.Location = new System.Drawing.Point(76, 445);
            this.token6.Name = "token6";
            this.token6.Size = new System.Drawing.Size(32, 20);
            this.token6.TabIndex = 62;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(60, 447);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(16, 13);
            this.label22.TabIndex = 61;
            this.label22.Text = "6:";
            // 
            // token5
            // 
            this.token5.Location = new System.Drawing.Point(18, 523);
            this.token5.Name = "token5";
            this.token5.Size = new System.Drawing.Size(32, 20);
            this.token5.TabIndex = 60;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(4, 525);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(16, 13);
            this.label18.TabIndex = 59;
            this.label18.Text = "5:";
            // 
            // token3
            // 
            this.token3.Location = new System.Drawing.Point(19, 471);
            this.token3.Name = "token3";
            this.token3.Size = new System.Drawing.Size(32, 20);
            this.token3.TabIndex = 58;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 473);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(16, 13);
            this.label17.TabIndex = 57;
            this.label17.Text = "3:";
            // 
            // token4
            // 
            this.token4.Location = new System.Drawing.Point(19, 497);
            this.token4.Name = "token4";
            this.token4.Size = new System.Drawing.Size(32, 20);
            this.token4.TabIndex = 56;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(4, 499);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(16, 13);
            this.label16.TabIndex = 55;
            this.label16.Text = "4:";
            // 
            // token2
            // 
            this.token2.Location = new System.Drawing.Point(19, 445);
            this.token2.Name = "token2";
            this.token2.Size = new System.Drawing.Size(32, 20);
            this.token2.TabIndex = 54;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 447);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(16, 13);
            this.label15.TabIndex = 53;
            this.label15.Text = "2:";
            // 
            // radioButtonRandom
            // 
            this.radioButtonRandom.AutoSize = true;
            this.radioButtonRandom.Location = new System.Drawing.Point(82, 343);
            this.radioButtonRandom.Name = "radioButtonRandom";
            this.radioButtonRandom.Size = new System.Drawing.Size(52, 17);
            this.radioButtonRandom.TabIndex = 52;
            this.radioButtonRandom.TabStop = true;
            this.radioButtonRandom.Text = "Place";
            this.radioButtonRandom.UseVisualStyleBackColor = true;
            // 
            // radioButtonGold
            // 
            this.radioButtonGold.AutoSize = true;
            this.radioButtonGold.Location = new System.Drawing.Point(115, 318);
            this.radioButtonGold.Name = "radioButtonGold";
            this.radioButtonGold.Size = new System.Drawing.Size(52, 17);
            this.radioButtonGold.TabIndex = 51;
            this.radioButtonGold.TabStop = true;
            this.radioButtonGold.Text = "Place";
            this.radioButtonGold.UseVisualStyleBackColor = true;
            // 
            // radioButtonSpice
            // 
            this.radioButtonSpice.AutoSize = true;
            this.radioButtonSpice.Location = new System.Drawing.Point(115, 292);
            this.radioButtonSpice.Name = "radioButtonSpice";
            this.radioButtonSpice.Size = new System.Drawing.Size(52, 17);
            this.radioButtonSpice.TabIndex = 50;
            this.radioButtonSpice.TabStop = true;
            this.radioButtonSpice.Text = "Place";
            this.radioButtonSpice.UseVisualStyleBackColor = true;
            // 
            // radioButtonFish
            // 
            this.radioButtonFish.AutoSize = true;
            this.radioButtonFish.Location = new System.Drawing.Point(115, 267);
            this.radioButtonFish.Name = "radioButtonFish";
            this.radioButtonFish.Size = new System.Drawing.Size(52, 17);
            this.radioButtonFish.TabIndex = 49;
            this.radioButtonFish.TabStop = true;
            this.radioButtonFish.Text = "Place";
            this.radioButtonFish.UseVisualStyleBackColor = true;
            // 
            // radioButtonCouncil
            // 
            this.radioButtonCouncil.AutoSize = true;
            this.radioButtonCouncil.Location = new System.Drawing.Point(115, 241);
            this.radioButtonCouncil.Name = "radioButtonCouncil";
            this.radioButtonCouncil.Size = new System.Drawing.Size(52, 17);
            this.radioButtonCouncil.TabIndex = 48;
            this.radioButtonCouncil.TabStop = true;
            this.radioButtonCouncil.Text = "Place";
            this.radioButtonCouncil.UseVisualStyleBackColor = true;
            // 
            // radioButtonDesert
            // 
            this.radioButtonDesert.AutoSize = true;
            this.radioButtonDesert.Location = new System.Drawing.Point(115, 215);
            this.radioButtonDesert.Name = "radioButtonDesert";
            this.radioButtonDesert.Size = new System.Drawing.Size(52, 17);
            this.radioButtonDesert.TabIndex = 47;
            this.radioButtonDesert.TabStop = true;
            this.radioButtonDesert.Text = "Place";
            this.radioButtonDesert.UseVisualStyleBackColor = true;
            // 
            // radioButtonForest
            // 
            this.radioButtonForest.AutoSize = true;
            this.radioButtonForest.Location = new System.Drawing.Point(115, 189);
            this.radioButtonForest.Name = "radioButtonForest";
            this.radioButtonForest.Size = new System.Drawing.Size(52, 17);
            this.radioButtonForest.TabIndex = 46;
            this.radioButtonForest.TabStop = true;
            this.radioButtonForest.Text = "Place";
            this.radioButtonForest.UseVisualStyleBackColor = true;
            // 
            // radioButtonHills
            // 
            this.radioButtonHills.AutoSize = true;
            this.radioButtonHills.Location = new System.Drawing.Point(115, 166);
            this.radioButtonHills.Name = "radioButtonHills";
            this.radioButtonHills.Size = new System.Drawing.Size(52, 17);
            this.radioButtonHills.TabIndex = 45;
            this.radioButtonHills.TabStop = true;
            this.radioButtonHills.Text = "Place";
            this.radioButtonHills.UseVisualStyleBackColor = true;
            // 
            // radioButtonMountians
            // 
            this.radioButtonMountians.AutoSize = true;
            this.radioButtonMountians.Location = new System.Drawing.Point(126, 139);
            this.radioButtonMountians.Name = "radioButtonMountians";
            this.radioButtonMountians.Size = new System.Drawing.Size(52, 17);
            this.radioButtonMountians.TabIndex = 44;
            this.radioButtonMountians.TabStop = true;
            this.radioButtonMountians.Text = "Place";
            this.radioButtonMountians.UseVisualStyleBackColor = true;
            // 
            // radioButtonPasture
            // 
            this.radioButtonPasture.AutoSize = true;
            this.radioButtonPasture.Location = new System.Drawing.Point(126, 113);
            this.radioButtonPasture.Name = "radioButtonPasture";
            this.radioButtonPasture.Size = new System.Drawing.Size(52, 17);
            this.radioButtonPasture.TabIndex = 43;
            this.radioButtonPasture.TabStop = true;
            this.radioButtonPasture.Text = "Place";
            this.radioButtonPasture.UseVisualStyleBackColor = true;
            // 
            // radioButtonFields
            // 
            this.radioButtonFields.AutoSize = true;
            this.radioButtonFields.Location = new System.Drawing.Point(115, 87);
            this.radioButtonFields.Name = "radioButtonFields";
            this.radioButtonFields.Size = new System.Drawing.Size(52, 17);
            this.radioButtonFields.TabIndex = 42;
            this.radioButtonFields.TabStop = true;
            this.radioButtonFields.Text = "Place";
            this.radioButtonFields.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 322);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(32, 13);
            this.label13.TabIndex = 41;
            this.label13.Text = "Gold:";
            // 
            // GoldCount
            // 
            this.GoldCount.Location = new System.Drawing.Point(51, 318);
            this.GoldCount.Name = "GoldCount";
            this.GoldCount.Size = new System.Drawing.Size(58, 20);
            this.GoldCount.TabIndex = 40;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 294);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(37, 13);
            this.label14.TabIndex = 39;
            this.label14.Text = "Spice:";
            // 
            // SpiceCount
            // 
            this.SpiceCount.Location = new System.Drawing.Point(51, 292);
            this.SpiceCount.Name = "SpiceCount";
            this.SpiceCount.Size = new System.Drawing.Size(58, 20);
            this.SpiceCount.TabIndex = 38;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(33, 418);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 37;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // checkBoxBeachTiles
            // 
            this.checkBoxBeachTiles.Location = new System.Drawing.Point(15, 368);
            this.checkBoxBeachTiles.MaximumSize = new System.Drawing.Size(200, 200);
            this.checkBoxBeachTiles.MinimumSize = new System.Drawing.Size(180, 50);
            this.checkBoxBeachTiles.Name = "checkBoxBeachTiles";
            this.checkBoxBeachTiles.Size = new System.Drawing.Size(193, 50);
            this.checkBoxBeachTiles.TabIndex = 36;
            this.checkBoxBeachTiles.Text = "Ensure Spice, Pirate Lairs, and Fish, Green Moon and Orange Sun tiles are next to" +
    " water";
            this.checkBoxBeachTiles.UseVisualStyleBackColor = true;
            // 
            // radioButtonOrangeSun
            // 
            this.radioButtonOrangeSun.AutoSize = true;
            this.radioButtonOrangeSun.Location = new System.Drawing.Point(140, 59);
            this.radioButtonOrangeSun.Name = "radioButtonOrangeSun";
            this.radioButtonOrangeSun.Size = new System.Drawing.Size(52, 17);
            this.radioButtonOrangeSun.TabIndex = 26;
            this.radioButtonOrangeSun.TabStop = true;
            this.radioButtonOrangeSun.Text = "Place";
            this.radioButtonOrangeSun.UseVisualStyleBackColor = true;
            // 
            // radioButtonGreenMoon
            // 
            this.radioButtonGreenMoon.AutoSize = true;
            this.radioButtonGreenMoon.Location = new System.Drawing.Point(140, 34);
            this.radioButtonGreenMoon.Name = "radioButtonGreenMoon";
            this.radioButtonGreenMoon.Size = new System.Drawing.Size(52, 17);
            this.radioButtonGreenMoon.TabIndex = 25;
            this.radioButtonGreenMoon.TabStop = true;
            this.radioButtonGreenMoon.Text = "Place";
            this.radioButtonGreenMoon.UseVisualStyleBackColor = true;
            // 
            // radioButtonSea
            // 
            this.radioButtonSea.AutoSize = true;
            this.radioButtonSea.Location = new System.Drawing.Point(115, 10);
            this.radioButtonSea.Name = "radioButtonSea";
            this.radioButtonSea.Size = new System.Drawing.Size(52, 17);
            this.radioButtonSea.TabIndex = 24;
            this.radioButtonSea.TabStop = true;
            this.radioButtonSea.Text = "Place";
            this.radioButtonSea.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 345);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "Random";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 271);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Fish:";
            // 
            // FishCount
            // 
            this.FishCount.Location = new System.Drawing.Point(51, 267);
            this.FishCount.Name = "FishCount";
            this.FishCount.Size = new System.Drawing.Size(58, 20);
            this.FishCount.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(0, 243);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "Council:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(10, 217);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 13);
            this.label12.TabIndex = 18;
            this.label12.Text = "Desert:";
            // 
            // CouncilCount
            // 
            this.CouncilCount.Location = new System.Drawing.Point(51, 241);
            this.CouncilCount.Name = "CouncilCount";
            this.CouncilCount.Size = new System.Drawing.Size(58, 20);
            this.CouncilCount.TabIndex = 17;
            // 
            // DesertCount
            // 
            this.DesertCount.Location = new System.Drawing.Point(51, 215);
            this.DesertCount.Name = "DesertCount";
            this.DesertCount.Size = new System.Drawing.Size(58, 20);
            this.DesertCount.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 191);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Forest:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 165);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Hills:";
            // 
            // ForestCount
            // 
            this.ForestCount.Location = new System.Drawing.Point(51, 189);
            this.ForestCount.Name = "ForestCount";
            this.ForestCount.Size = new System.Drawing.Size(58, 20);
            this.ForestCount.TabIndex = 13;
            // 
            // HillsCount
            // 
            this.HillsCount.Location = new System.Drawing.Point(51, 163);
            this.HillsCount.Name = "HillsCount";
            this.HillsCount.Size = new System.Drawing.Size(58, 20);
            this.HillsCount.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 141);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Mountains:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 113);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Pasture:";
            // 
            // MountainsCount
            // 
            this.MountainsCount.Location = new System.Drawing.Point(62, 137);
            this.MountainsCount.Name = "MountainsCount";
            this.MountainsCount.Size = new System.Drawing.Size(58, 20);
            this.MountainsCount.TabIndex = 9;
            // 
            // PastureCount
            // 
            this.PastureCount.Location = new System.Drawing.Point(62, 111);
            this.PastureCount.Name = "PastureCount";
            this.PastureCount.Size = new System.Drawing.Size(58, 20);
            this.PastureCount.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Fields:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Orange Sun:";
            // 
            // FieldsCount
            // 
            this.FieldsCount.Location = new System.Drawing.Point(51, 85);
            this.FieldsCount.Name = "FieldsCount";
            this.FieldsCount.Size = new System.Drawing.Size(58, 20);
            this.FieldsCount.TabIndex = 5;
            // 
            // OrangeSunCount
            // 
            this.OrangeSunCount.Location = new System.Drawing.Point(76, 59);
            this.OrangeSunCount.Name = "OrangeSunCount";
            this.OrangeSunCount.Size = new System.Drawing.Size(58, 20);
            this.OrangeSunCount.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Green Moon:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Sea:";
            // 
            // GreenMoonCount
            // 
            this.GreenMoonCount.Location = new System.Drawing.Point(76, 33);
            this.GreenMoonCount.Name = "GreenMoonCount";
            this.GreenMoonCount.Size = new System.Drawing.Size(58, 20);
            this.GreenMoonCount.TabIndex = 1;
            // 
            // SeaCount
            // 
            this.SeaCount.Location = new System.Drawing.Point(51, 7);
            this.SeaCount.Name = "SeaCount";
            this.SeaCount.Size = new System.Drawing.Size(58, 20);
            this.SeaCount.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 611);
            this.Controls.Add(this.tableLayoutPanel1);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.token11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.token12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.token10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.token9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.token8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.token6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.token5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.token3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.token4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.token2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GoldCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpiceCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FishCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CouncilCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DesertCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ForestCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HillsCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MountainsCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PastureCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FieldsCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrangeSunCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GreenMoonCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SeaCount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown GreenMoonCount;
        private System.Windows.Forms.NumericUpDown SeaCount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown ForestCount;
        private System.Windows.Forms.NumericUpDown HillsCount;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown MountainsCount;
        private System.Windows.Forms.NumericUpDown PastureCount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown FieldsCount;
        private System.Windows.Forms.NumericUpDown OrangeSunCount;
        private System.Windows.Forms.CheckBox checkBoxBeachTiles;
        private System.Windows.Forms.RadioButton radioButtonOrangeSun;
        private System.Windows.Forms.RadioButton radioButtonGreenMoon;
        private System.Windows.Forms.RadioButton radioButtonSea;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown FishCount;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown CouncilCount;
        private System.Windows.Forms.NumericUpDown DesertCount;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown GoldCount;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown SpiceCount;
        private System.Windows.Forms.RadioButton radioButtonRandom;
        private System.Windows.Forms.RadioButton radioButtonGold;
        private System.Windows.Forms.RadioButton radioButtonSpice;
        private System.Windows.Forms.RadioButton radioButtonFish;
        private System.Windows.Forms.RadioButton radioButtonCouncil;
        private System.Windows.Forms.RadioButton radioButtonDesert;
        private System.Windows.Forms.RadioButton radioButtonForest;
        private System.Windows.Forms.RadioButton radioButtonHills;
        private System.Windows.Forms.RadioButton radioButtonMountians;
        private System.Windows.Forms.RadioButton radioButtonPasture;
        private System.Windows.Forms.RadioButton radioButtonFields;
        private System.Windows.Forms.NumericUpDown token11;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.NumericUpDown token12;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.NumericUpDown token10;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.NumericUpDown token9;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.NumericUpDown token8;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown token6;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.NumericUpDown token5;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown token3;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown token4;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown token2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label labelLeft;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CatanBoardMaker
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            _hexTreeBlueprint = new HexList(20);
            _hexTreeOutcome = new HexList(20);
            pictureBox1.Refresh();
            _random = new Random();
        }
        HexList _hexTreeBlueprint;
        HexList _hexTreeOutcome;
        Random _random;
        bool _moving;
        bool _shiftDown;
        int _previousMouseX;
        int _previousMouseY;
        private void button1_Click(object sender, EventArgs e)
        {
            //hex.PrintPoints();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            /*if (hex.ContainsPoint(new Point(e.X,e.Y)))
            {
                hex.changeColour();
            }*/
            if (!_shiftDown)
            {
                if (e.Button == MouseButtons.Left)
                {
                    if (radioButtonCouncil.Checked)
                    {
                        _hexTreeBlueprint.Add(new Point(e.X, e.Y), HexType.COUNCIL);
                        _hexTreeOutcome.Add(new Point(e.X, e.Y), HexType.COUNCIL);
                    }
                    else if (radioButtonDesert.Checked)
                    {
                        _hexTreeBlueprint.Add(new Point(e.X, e.Y), HexType.DESERT);
                        _hexTreeOutcome.Add(new Point(e.X, e.Y), HexType.DESERT);
                    }
                    else if (radioButtonFields.Checked)
                    {
                        _hexTreeBlueprint.Add(new Point(e.X, e.Y), HexType.FIELDS);
                        _hexTreeOutcome.Add(new Point(e.X, e.Y), HexType.FIELDS);
                    }
                    else if (radioButtonFish.Checked)
                    {
                        _hexTreeBlueprint.Add(new Point(e.X, e.Y), HexType.FISH);
                        _hexTreeOutcome.Add(new Point(e.X, e.Y), HexType.FISH);
                    }
                    else if (radioButtonForest.Checked)
                    {
                        _hexTreeBlueprint.Add(new Point(e.X, e.Y), HexType.FOREST);
                        _hexTreeOutcome.Add(new Point(e.X, e.Y), HexType.FOREST);
                    }
                    else if (radioButtonGold.Checked)
                    {
                        _hexTreeBlueprint.Add(new Point(e.X, e.Y), HexType.GOLD);
                        _hexTreeOutcome.Add(new Point(e.X, e.Y), HexType.GOLD);
                    }
                    else if (radioButtonGreenMoon.Checked)
                    {
                        _hexTreeBlueprint.Add(new Point(e.X, e.Y), HexType.GREEN_MOON);
                        _hexTreeOutcome.Add(new Point(e.X, e.Y), HexType.GREEN_MOON);
                    }
                    else if (radioButtonHills.Checked)
                    {
                        _hexTreeBlueprint.Add(new Point(e.X, e.Y), HexType.HILLS);
                        _hexTreeOutcome.Add(new Point(e.X, e.Y), HexType.HILLS);
                    }
                    else if (radioButtonMountians.Checked)
                    {
                        _hexTreeBlueprint.Add(new Point(e.X, e.Y), HexType.MOUNTAINS);
                        _hexTreeOutcome.Add(new Point(e.X, e.Y), HexType.MOUNTAINS);
                    }
                    else if (radioButtonOrangeSun.Checked)
                    {
                        _hexTreeBlueprint.Add(new Point(e.X, e.Y), HexType.ORANGE_SUN);
                        _hexTreeOutcome.Add(new Point(e.X, e.Y), HexType.ORANGE_SUN);
                    }
                    else if (radioButtonPasture.Checked)
                    {
                        _hexTreeBlueprint.Add(new Point(e.X, e.Y), HexType.PASTURE);
                        _hexTreeOutcome.Add(new Point(e.X, e.Y), HexType.PASTURE);
                    }
                    else if (radioButtonRandom.Checked)
                    {
                        _hexTreeBlueprint.Add(new Point(e.X, e.Y), HexType.EMPTY);
                        _hexTreeOutcome.Add(new Point(e.X, e.Y), HexType.EMPTY);
                    }
                    else if (radioButtonSea.Checked)
                    {
                        _hexTreeBlueprint.Add(new Point(e.X, e.Y), HexType.SEA);
                        _hexTreeOutcome.Add(new Point(e.X, e.Y), HexType.SEA);
                    }
                    else if (radioButtonSpice.Checked)
                    {
                        _hexTreeBlueprint.Add(new Point(e.X, e.Y), HexType.SPICE);
                        _hexTreeOutcome.Add(new Point(e.X, e.Y), HexType.SPICE);
                    }
                    
                }
                else
                {
                    Hex foundHex = _hexTreeBlueprint.Find(new Point(e.X, e.Y));
                    if (foundHex != null)
                    {
                        foundHex.Type = HexType.SEA;
                    }
                    foundHex = _hexTreeOutcome.Find(new Point(e.X, e.Y));
                    if (foundHex != null)
                    {
                        foundHex.Type = HexType.SEA;
                    }
                }
            }
            //MessageBox.Show(_hexTree.Trunk.DistanceFromCentre(new Point(e.X, e.Y)).ToString());
            pictureBox1.Refresh();
            pictureBox2.Refresh();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            //hex.Draw(e.Graphics);
            _hexTreeBlueprint.Draw(e.Graphics);
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            _moving = true;
            _previousMouseX = e.X;
            _previousMouseY = e.Y;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (_moving&&_shiftDown)
            {
                _hexTreeBlueprint.Move(e.X - _previousMouseX, e.Y - _previousMouseY);
                _hexTreeOutcome.Move(e.X - _previousMouseX, e.Y - _previousMouseY);
                _previousMouseX = e.X;
                _previousMouseY = e.Y;
            }
            pictureBox1.Refresh();
            pictureBox2.Refresh();
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            _moving = false;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            _shiftDown = e.Shift;
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            _shiftDown = e.Shift;
        }

        private void pictureBox2_Paint(object sender, PaintEventArgs e)
        {
            _hexTreeOutcome.Draw(e.Graphics);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            _hexTreeOutcome = new HexList(_hexTreeBlueprint);
            int totalCount = 0;
            int seaCount = (int)SeaCount.Value;
            totalCount += seaCount;
            int desertCount = (int)DesertCount.Value;
            totalCount += desertCount;
            int mountainsCount = (int)MountainsCount.Value;
            totalCount += mountainsCount;
            int hillsCount = (int)HillsCount.Value;
            totalCount += hillsCount;
            int forestCount = (int)ForestCount.Value;
            totalCount += forestCount;
            int fishCount = (int)FishCount.Value;
            totalCount += fishCount;
            int spiceCount = (int)SpiceCount.Value;
            totalCount += spiceCount;
            int fieldsCount = (int)FieldsCount.Value;
            totalCount += fieldsCount;
            int goldCount = (int)GoldCount.Value;
            totalCount += goldCount;
            int orangeSunCount = (int)OrangeSunCount.Value;
            totalCount += orangeSunCount;
            int greenMoonCount = (int)GreenMoonCount.Value;
            totalCount += greenMoonCount;
            int councilCount = (int)CouncilCount.Value;
            totalCount += councilCount;
            int pastureCount = (int)PastureCount.Value;
            totalCount += pastureCount;
            int emptyCount = _hexTreeOutcome.Count(HexType.EMPTY);
            if (totalCount >= emptyCount)
            {
                while (emptyCount > 0)
                {
                    int roll = _random.Next(1, totalCount + 1);
                    int rollingCount = 0;
                    if (roll <= (rollingCount+=seaCount))
                    {
                        _hexTreeOutcome.GetFirst(HexType.EMPTY).Type = HexType.SEA;
                        seaCount--;
                        totalCount--;
                        emptyCount--;
                    }
                    else if (roll <= (rollingCount+=desertCount))
                    {
                        _hexTreeOutcome.GetFirst(HexType.EMPTY).Type = HexType.DESERT;
                        desertCount--;
                        totalCount--;
                        emptyCount--;
                    }
                    else if (roll <= (rollingCount += mountainsCount))
                    {
                        _hexTreeOutcome.GetFirst(HexType.EMPTY).Type = HexType.MOUNTAINS;
                        mountainsCount--;
                        totalCount--;
                        emptyCount--;
                    }
                    else if (roll <= (rollingCount += hillsCount))
                    {
                        _hexTreeOutcome.GetFirst(HexType.EMPTY).Type = HexType.HILLS;
                        hillsCount--;
                        totalCount--;
                        emptyCount--;
                    }
                    else if (roll <= (rollingCount += forestCount))
                    {
                        _hexTreeOutcome.GetFirst(HexType.EMPTY).Type = HexType.FOREST;
                        forestCount--;
                        totalCount--;
                        emptyCount--;
                    }
                    else if (roll <= (rollingCount += fishCount))
                    {
                        _hexTreeOutcome.GetFirst(HexType.EMPTY).Type = HexType.FISH;
                        fishCount--;
                        totalCount--;
                        emptyCount--;
                    }
                    else if (roll <= (rollingCount += spiceCount))
                    {
                        _hexTreeOutcome.GetFirst(HexType.EMPTY).Type = HexType.SPICE;
                        spiceCount--;
                        totalCount--;
                        emptyCount--;
                    }
                    else if (roll <= (rollingCount += fieldsCount))
                    {
                        _hexTreeOutcome.GetFirst(HexType.EMPTY).Type = HexType.FIELDS;
                        fieldsCount--;
                        totalCount--;
                        emptyCount--;
                    }
                    else if (roll <= (rollingCount += goldCount))
                    {
                        _hexTreeOutcome.GetFirst(HexType.EMPTY).Type = HexType.GOLD;
                        goldCount--;
                        totalCount--;
                        emptyCount--;
                    }
                    else if (roll <= (rollingCount += orangeSunCount))
                    {
                        _hexTreeOutcome.GetFirst(HexType.EMPTY).Type = HexType.ORANGE_SUN;
                        orangeSunCount--;
                        totalCount--;
                        emptyCount--;
                    }
                    else if (roll <= (rollingCount += greenMoonCount))
                    {
                        _hexTreeOutcome.GetFirst(HexType.EMPTY).Type = HexType.GREEN_MOON;
                        greenMoonCount--;
                        totalCount--;
                        emptyCount--;
                    }
                    else if (roll <= (rollingCount += councilCount))
                    {
                        _hexTreeOutcome.GetFirst(HexType.EMPTY).Type = HexType.COUNCIL;
                        councilCount--;
                        totalCount--;
                        emptyCount--;
                    }
                    else if (roll <= (rollingCount += pastureCount))
                    {
                        _hexTreeOutcome.GetFirst(HexType.EMPTY).Type = HexType.PASTURE;
                        pastureCount--;
                        totalCount--;
                        emptyCount--;
                    }
                }
                labelLeft.Text = "Leftover:";
                if (pastureCount > 0)
                {
                    labelLeft.Text += " " + pastureCount + " Pasture";
                }
                if (councilCount > 0)
                {
                    labelLeft.Text += " " + councilCount + " council";
                }
                if (desertCount > 0)
                {
                    labelLeft.Text += " " + desertCount + " desert";
                }
                if (fieldsCount > 0)
                {
                    labelLeft.Text += " " + fieldsCount + " fields";
                }
                if (forestCount > 0)
                {
                    labelLeft.Text += " " + forestCount + " forest";
                }
                if (goldCount > 0)
                {
                    labelLeft.Text += " " + goldCount + " gold";
                }
                if (mountainsCount > 0)
                {
                    labelLeft.Text += " " + mountainsCount + " mountains";
                }
                if (spiceCount > 0)
                {
                    labelLeft.Text += " " + spiceCount + " spice";
                }
                if (fishCount > 0)
                {
                    labelLeft.Text += " " + fishCount + " fish";
                }
                if (hillsCount > 0)
                {
                    labelLeft.Text += " " + hillsCount + " hills";
                }
                if (seaCount > 0)
                {
                    labelLeft.Text += " " + seaCount + " sea";
                }
                if (orangeSunCount > 0)
                {
                    labelLeft.Text += " " + orangeSunCount + " Orange Sun";
                }
                if (greenMoonCount > 0)
                {
                    labelLeft.Text += " " + greenMoonCount + " Green Moon";
                }
                PlaceNumbers();
                pictureBox2.Refresh();
            }
            else
            {
                MessageBox.Show("Not enough tiles were supplied! There were "+_hexTreeOutcome.Count(HexType.EMPTY)+" empty tiles and "+totalCount+" total tiles");
            }
        }

        private void PlaceNumbers()
        {
            _hexTreeOutcome.ResetNumbers();
            int totalTokens = 0;
            int token2s = (int)token2.Value;
            totalTokens += token2s;
            int token3s = (int)token3.Value;
            totalTokens += token3s;
            int token4s = (int)token4.Value;
            totalTokens += token4s;
            int token5s = (int)token5.Value;
            totalTokens += token5s;
            int token6s = (int)token6.Value;
            totalTokens += token6s;
            int token8s = (int)token8.Value;
            totalTokens += token8s;
            int token9s = (int)token9.Value;
            totalTokens += token9s;
            int token10s = (int)token10.Value;
            totalTokens += token10s;
            int token11s = (int)token11.Value;
            totalTokens += token11s;
            int token12s = (int)token12.Value;
            totalTokens += token12s;
            int numUntokened = _hexTreeOutcome.NumUntokened;
            //MessageBox.Show("numUntokened: " + numUntokened + " totalTokens: " + totalTokens);
            if (totalTokens >= numUntokened)
            {
                int redRepeatCount = 0;
                while (numUntokened > 0)
                {
                    if (redRepeatCount > 5)
                    {
                        //MessageBox.Show("ran into 5 repeats");
                        PlaceNumbers();
                        return;
                    }
                    int roll = _random.Next(1, totalTokens + 1);
                    int rollingCount = 0;
                    if (roll <= (rollingCount += token2s))
                    {
                        _hexTreeOutcome.FirstUntokened.TokenNumber = 2;
                        token2s--;
                        totalTokens--;
                        numUntokened--;
                        redRepeatCount = 0;
                    }
                    else if (roll <= (rollingCount += token3s))
                    {
                        _hexTreeOutcome.FirstUntokened.TokenNumber = 3;
                        token3s--;
                        totalTokens--;
                        numUntokened--;
                        redRepeatCount = 0;
                    }
                    else if (roll <= (rollingCount += token4s))
                    {
                        _hexTreeOutcome.FirstUntokened.TokenNumber = 4;
                        token4s--;
                        totalTokens--;
                        numUntokened--;
                        redRepeatCount = 0;
                    }
                    else if (roll <= (rollingCount += token5s))
                    {
                        _hexTreeOutcome.FirstUntokened.TokenNumber = 5;
                        token5s--;
                        totalTokens--;
                        numUntokened--;
                        redRepeatCount = 0;
                    }
                    else if (roll <= (rollingCount += token6s))
                    {
                        Hex firstUntokened = _hexTreeOutcome.FirstUntokened;
                        if (firstUntokened.CanHoldRedToken)
                        {
                            firstUntokened.TokenNumber = 6;
                            token6s--;
                            totalTokens--;
                            numUntokened--;
                            redRepeatCount = 0;
                        }
                        else
                        {
                            redRepeatCount += 1;
                        }
                    }
                    else if (roll <= (rollingCount += token8s))
                    {
                        Hex firstUntokened = _hexTreeOutcome.FirstUntokened;
                        if (firstUntokened.CanHoldRedToken)
                        {
                            firstUntokened.TokenNumber = 8;
                            token8s--;
                            totalTokens--;
                            numUntokened--;
                            redRepeatCount = 0;
                        }
                        else
                        {
                            redRepeatCount += 1;
                        }
                    }
                    else if (roll <= (rollingCount += token9s))
                    {
                        _hexTreeOutcome.FirstUntokened.TokenNumber = 9;
                        token9s--;
                        totalTokens--;
                        numUntokened--;
                        redRepeatCount = 0;
                    }
                    else if (roll <= (rollingCount += token10s))
                    {
                        _hexTreeOutcome.FirstUntokened.TokenNumber = 10;
                        token10s--;
                        totalTokens--;
                        numUntokened--;
                        redRepeatCount = 0;
                    }
                    else if (roll <= (rollingCount += token11s))
                    {
                        _hexTreeOutcome.FirstUntokened.TokenNumber = 11;
                        token11s--;
                        totalTokens--;
                        numUntokened--;
                        redRepeatCount = 0;
                    }
                    else if (roll <= (rollingCount += token12s))
                    {
                        _hexTreeOutcome.FirstUntokened.TokenNumber = 12;
                        token12s--;
                        totalTokens--;
                        numUntokened--;
                        redRepeatCount = 0;
                    }
                }
            }
            else
            {
                MessageBox.Show("Not enough tokens were supplied to cover the tiles!");
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    //base
                    { 
                    _hexTreeBlueprint = new HexList(20);
                    _hexTreeBlueprint.Add(new Point(137, 270), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(171, 270), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(215, 270), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(130, 300), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(164, 300), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(198, 300), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(232, 300), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(249, 330), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(215, 330), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(171, 330), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(137, 330), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(103, 330), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(130, 360), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(164, 360), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(198, 360), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(232, 360), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(137, 390), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(171, 390), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(215, 390), HexType.EMPTY);
                    _hexTreeOutcome = new HexList(_hexTreeBlueprint);

                    SeaCount.Value = 0;
                    GreenMoonCount.Value = 0;
                    OrangeSunCount.Value = 0;
                    FieldsCount.Value = 4;
                    PastureCount.Value = 4;
                    MountainsCount.Value = 3;
                    HillsCount.Value = 3;
                    ForestCount.Value = 4;
                    DesertCount.Value = 1;
                    FishCount.Value = 0;
                    CouncilCount.Value = 0;
                    SpiceCount.Value = 0;
                    GoldCount.Value = 0;

                    token2.Value = 1;
                    token3.Value = 2;
                    token4.Value = 2;
                    token5.Value = 2;
                    token6.Value = 2;
                    token8.Value = 2;
                    token9.Value = 2;
                    token10.Value = 2;
                    token11.Value = 2;
                    token12.Value = 1;
            }
                    break;
                case 1:
                    //base extension
                    /*string pointsList = "";
                    foreach (Hex hex in _hexTreeBlueprint)
                    {
                        pointsList += hex.Centre.X + "," + hex.Centre.Y + " ";
                    }
                    MessageBox.Show(pointsList);
                    */
                    { 
                    _hexTreeBlueprint = new HexList(20);

                    _hexTreeBlueprint.Add(new Point(164, 240), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(198, 240), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(232, 240), HexType.EMPTY);

                    _hexTreeBlueprint.Add(new Point(137, 270), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(171, 270), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(215, 270), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(249, 270), HexType.EMPTY);

                    _hexTreeBlueprint.Add(new Point(130, 300), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(164, 300), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(198, 300), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(232, 300), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(266, 300), HexType.EMPTY);

                    _hexTreeBlueprint.Add(new Point(249, 330), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(215, 330), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(171, 330), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(137, 330), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(103, 330), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(283, 330), HexType.EMPTY);

                    _hexTreeBlueprint.Add(new Point(130, 360), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(164, 360), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(198, 360), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(232, 360), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(266, 360), HexType.EMPTY);

                    _hexTreeBlueprint.Add(new Point(137, 390), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(171, 390), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(215, 390), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(249, 390), HexType.EMPTY);

                    _hexTreeBlueprint.Add(new Point(164, 420), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(198, 420), HexType.EMPTY);
                    _hexTreeBlueprint.Add(new Point(232, 420), HexType.EMPTY);

                    _hexTreeOutcome = new HexList(_hexTreeBlueprint);

                    SeaCount.Value = 0;
                    GreenMoonCount.Value = 0;
                    OrangeSunCount.Value = 0;
                    FieldsCount.Value = 6;
                    PastureCount.Value = 6;
                    MountainsCount.Value = 5;
                    HillsCount.Value = 5;
                    ForestCount.Value = 6;
                    DesertCount.Value = 2;
                    FishCount.Value = 0;
                    CouncilCount.Value = 0;
                    SpiceCount.Value = 0;
                    GoldCount.Value = 0;

                    token2.Value = 2;
                    token3.Value = 3;
                    token4.Value = 3;
                    token5.Value = 3;
                    token6.Value = 3;
                    token8.Value = 3;
                    token9.Value = 3;
                    token10.Value = 3;
                    token11.Value = 3;
                    token12.Value = 2;
            }
                    break;
                case 2:
                    //102
                    {
                        _hexTreeBlueprint = new HexList(20);
                        
                        _hexTreeBlueprint.Add(new Point(198, 180), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 180), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(266, 180), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(300, 180), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(334, 180), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(368, 180), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(402, 180), HexType.EMPTY);

                        _hexTreeBlueprint.Add(new Point(171, 210), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 210), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(249, 210), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(283, 210), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(317, 210), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(351, 210), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(385, 210), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(419, 210), HexType.EMPTY);

                        _hexTreeBlueprint.Add(new Point(164, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(266, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(300, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(334, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(368, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(402, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(436, 240), HexType.EMPTY);

                        _hexTreeBlueprint.Add(new Point(137, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(249, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(283, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(317, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(351, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(385, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(419, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(453, 270), HexType.EMPTY);

                        _hexTreeBlueprint.Add(new Point(130, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(164, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(266, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(300, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(334, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(368, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(402, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(436, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(470, 300), HexType.EMPTY);

                        _hexTreeBlueprint.Add(new Point(103, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(137, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(249, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(283, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(317, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(351, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(385, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(419, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(453, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(487, 330), HexType.EMPTY);

                        _hexTreeBlueprint.Add(new Point(130, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(164, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(266, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(300, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(334, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(368, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(402, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(436, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(470, 360), HexType.EMPTY);

                        _hexTreeBlueprint.Add(new Point(137, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(249, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(283, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(317, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(351, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(385, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(419, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(453, 390), HexType.EMPTY);

                        _hexTreeBlueprint.Add(new Point(164, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(266, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(300, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(334, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(368, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(402, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(436, 420), HexType.EMPTY);

                        _hexTreeBlueprint.Add(new Point(171, 450), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 450), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(249, 450), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(283, 450), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(317, 450), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(351, 450), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(385, 450), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(419, 450), HexType.EMPTY);

                        _hexTreeBlueprint.Add(new Point(198, 480), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 480), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(266, 480), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(300, 480), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(334, 480), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(368, 480), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(402, 480), HexType.EMPTY);

                        _hexTreeOutcome = new HexList(_hexTreeBlueprint);

                        SeaCount.Value = 36;
                        GreenMoonCount.Value = 0;
                        OrangeSunCount.Value = 0;
                        FieldsCount.Value = 10;
                        PastureCount.Value = 9;
                        MountainsCount.Value = 10;
                        HillsCount.Value = 8;
                        ForestCount.Value = 9;
                        DesertCount.Value = 2;
                        FishCount.Value = 6;
                        CouncilCount.Value = 2;
                        SpiceCount.Value = 6;
                        GoldCount.Value = 8;

                        token2.Value = 4;
                        token3.Value = 7;
                        token4.Value = 8;
                        token5.Value = 8;
                        token6.Value = 6;
                        token8.Value = 6;
                        token9.Value = 8;
                        token10.Value = 8;
                        token11.Value = 6;
                        token12.Value = 3;
                    }
                    break;
                case 3:
                    //E&P 1
                    {
                        _hexTreeBlueprint = new HexList(20);

                        _hexTreeBlueprint.Add(new Point(164, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 240), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 240), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(300, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(334, 240), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(137, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 270), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(249, 270), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(317, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(351, 270), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(130, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(164, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(232, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(300, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(334, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(368, 300), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(103, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(137, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(249, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(317, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(351, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(385, 330), HexType.SEA);

                        _hexTreeBlueprint.Add(new Point(130, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(164, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(232, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(300, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(334, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(368, 360), HexType.ORANGE_SUN);

                        _hexTreeBlueprint.Add(new Point(137, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 390), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(249, 390), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(317, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(351, 390), HexType.ORANGE_SUN);

                        _hexTreeBlueprint.Add(new Point(164, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 420), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 420), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(300, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(334, 420), HexType.ORANGE_SUN);

                        _hexTreeOutcome = new HexList(_hexTreeBlueprint);

                        SeaCount.Value = 0;
                        GreenMoonCount.Value = 0;
                        OrangeSunCount.Value = 0;
                        FieldsCount.Value = 2;
                        PastureCount.Value = 4;
                        MountainsCount.Value = 3;
                        HillsCount.Value = 2;
                        ForestCount.Value = 4;
                        DesertCount.Value = 0;
                        FishCount.Value = 0;
                        CouncilCount.Value = 0;
                        SpiceCount.Value = 0;
                        GoldCount.Value = 0;

                        token2.Value = 0;
                        token3.Value = 2;
                        token4.Value = 2;
                        token5.Value = 1;
                        token6.Value = 2;
                        token8.Value = 2;
                        token9.Value = 1;
                        token10.Value = 2;
                        token11.Value = 2;
                        token12.Value = 1;
                    }
                    break;
                case 4:
                    //E&P 2
                    {
                        _hexTreeBlueprint = new HexList(20);

                        _hexTreeBlueprint.Add(new Point(164, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 240), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 240), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(300, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(334, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(368, 240), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(137, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 270), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(249, 270), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(317, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(351, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(385, 270), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(130, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(164, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(232, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(300, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(334, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(368, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(402, 300), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(103, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(137, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(249, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(317, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(351, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(385, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(419, 330), HexType.SEA);

                        _hexTreeBlueprint.Add(new Point(130, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(164, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(232, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(300, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(334, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(368, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(402, 360), HexType.ORANGE_SUN);

                        _hexTreeBlueprint.Add(new Point(137, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 390), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(249, 390), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(317, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(351, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(385, 390), HexType.ORANGE_SUN);

                        _hexTreeBlueprint.Add(new Point(164, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 420), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 420), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(300, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(334, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(368, 420), HexType.ORANGE_SUN);

                        _hexTreeOutcome = new HexList(_hexTreeBlueprint);

                        SeaCount.Value = 0;
                        GreenMoonCount.Value = 0;
                        OrangeSunCount.Value = 0;
                        FieldsCount.Value = 2;
                        PastureCount.Value = 4;
                        MountainsCount.Value = 3;
                        HillsCount.Value = 2;
                        ForestCount.Value = 4;
                        DesertCount.Value = 0;
                        FishCount.Value = 0;
                        CouncilCount.Value = 0;
                        SpiceCount.Value = 0;
                        GoldCount.Value = 0;

                        token2.Value = 0;
                        token3.Value = 2;
                        token4.Value = 2;
                        token5.Value = 1;
                        token6.Value = 2;
                        token8.Value = 2;
                        token9.Value = 1;
                        token10.Value = 2;
                        token11.Value = 2;
                        token12.Value = 1;
                    }
                    break;
                case 5:
                    //E&P 3
                    {
                        _hexTreeBlueprint = new HexList(20);

                        _hexTreeBlueprint.Add(new Point(164, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 240), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 240), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(300, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(334, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(368, 240), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(137, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 270), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(249, 270), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(317, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(351, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(385, 270), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(130, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(164, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(232, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(300, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(334, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(368, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(402, 300), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(103, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(137, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 330), HexType.COUNCIL);
                        _hexTreeBlueprint.Add(new Point(249, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(317, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(351, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(385, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(419, 330), HexType.SEA);

                        _hexTreeBlueprint.Add(new Point(130, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(164, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(232, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(300, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(334, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(368, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(402, 360), HexType.ORANGE_SUN);

                        _hexTreeBlueprint.Add(new Point(137, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 390), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(249, 390), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(317, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(351, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(385, 390), HexType.ORANGE_SUN);

                        _hexTreeBlueprint.Add(new Point(164, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 420), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 420), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(300, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(334, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(368, 420), HexType.ORANGE_SUN);

                        _hexTreeOutcome = new HexList(_hexTreeBlueprint);

                        SeaCount.Value = 0;
                        GreenMoonCount.Value = 0;
                        OrangeSunCount.Value = 0;
                        FieldsCount.Value = 2;
                        PastureCount.Value = 4;
                        MountainsCount.Value = 3;
                        HillsCount.Value = 2;
                        ForestCount.Value = 4;
                        DesertCount.Value = 0;
                        FishCount.Value = 0;
                        CouncilCount.Value = 0;
                        SpiceCount.Value = 0;
                        GoldCount.Value = 0;

                        token2.Value = 0;
                        token3.Value = 2;
                        token4.Value = 2;
                        token5.Value = 1;
                        token6.Value = 2;
                        token8.Value = 2;
                        token9.Value = 1;
                        token10.Value = 2;
                        token11.Value = 2;
                        token12.Value = 1;
                    }
                    break;
                case 6:
                    //E&P 4
                    {
                        _hexTreeBlueprint = new HexList(20);

                        _hexTreeBlueprint.Add(new Point(164, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 240), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 240), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(300, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(334, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(368, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(402, 240), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(137, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 270), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(249, 270), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(317, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(351, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(385, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(419, 270), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(130, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(164, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(232, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(300, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(334, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(368, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(402, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(436, 300), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(103, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(137, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 330), HexType.COUNCIL);
                        _hexTreeBlueprint.Add(new Point(249, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(317, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(351, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(385, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(419, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(453, 330), HexType.SEA);

                        _hexTreeBlueprint.Add(new Point(130, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(164, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(232, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(300, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(334, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(368, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(402, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(436, 360), HexType.ORANGE_SUN);

                        _hexTreeBlueprint.Add(new Point(137, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 390), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(249, 390), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(317, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(351, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(385, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(419, 390), HexType.ORANGE_SUN);

                        _hexTreeBlueprint.Add(new Point(164, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 420), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 420), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(300, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(334, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(368, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(402, 420), HexType.ORANGE_SUN);

                        _hexTreeOutcome = new HexList(_hexTreeBlueprint);

                        SeaCount.Value = 0;
                        GreenMoonCount.Value = 0;
                        OrangeSunCount.Value = 0;
                        FieldsCount.Value = 2;
                        PastureCount.Value = 4;
                        MountainsCount.Value = 3;
                        HillsCount.Value = 2;
                        ForestCount.Value = 4;
                        DesertCount.Value = 0;
                        FishCount.Value = 0;
                        CouncilCount.Value = 0;
                        SpiceCount.Value = 0;
                        GoldCount.Value = 0;

                        token2.Value = 0;
                        token3.Value = 2;
                        token4.Value = 2;
                        token5.Value = 1;
                        token6.Value = 2;
                        token8.Value = 2;
                        token9.Value = 1;
                        token10.Value = 2;
                        token11.Value = 2;
                        token12.Value = 1;
                    }
                    break;
                case 7:
                    //E&P 5
                    {
                        _hexTreeBlueprint = new HexList(20);

                        _hexTreeBlueprint.Add(new Point(164, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 240), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 240), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(300, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(334, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(368, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(402, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(436, 240), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(137, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 270), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(249, 270), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(317, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(351, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(385, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(419, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(453, 270), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(130, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(164, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(232, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(300, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(334, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(368, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(402, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(436, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(470, 300), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(103, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(137, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 330), HexType.COUNCIL);
                        _hexTreeBlueprint.Add(new Point(249, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(317, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(351, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(385, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(419, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(453, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(487, 330), HexType.SEA);

                        _hexTreeBlueprint.Add(new Point(130, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(164, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(232, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(300, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(334, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(368, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(402, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(436, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(470, 360), HexType.ORANGE_SUN);

                        _hexTreeBlueprint.Add(new Point(137, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 390), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(249, 390), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(317, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(351, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(385, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(419, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(453, 390), HexType.ORANGE_SUN);

                        _hexTreeBlueprint.Add(new Point(164, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 420), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 420), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(300, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(334, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(368, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(402, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(436, 420), HexType.ORANGE_SUN);

                        _hexTreeOutcome = new HexList(_hexTreeBlueprint);

                        SeaCount.Value = 0;
                        GreenMoonCount.Value = 0;
                        OrangeSunCount.Value = 0;
                        FieldsCount.Value = 2;
                        PastureCount.Value = 4;
                        MountainsCount.Value = 3;
                        HillsCount.Value = 2;
                        ForestCount.Value = 4;
                        DesertCount.Value = 0;
                        FishCount.Value = 0;
                        CouncilCount.Value = 0;
                        SpiceCount.Value = 0;
                        GoldCount.Value = 0;

                        token2.Value = 0;
                        token3.Value = 2;
                        token4.Value = 2;
                        token5.Value = 1;
                        token6.Value = 2;
                        token8.Value = 2;
                        token9.Value = 1;
                        token10.Value = 2;
                        token11.Value = 2;
                        token12.Value = 1;
                    }
                    break;
                case 8:
                    //E&P 2 Extension
                    {
                        _hexTreeBlueprint = new HexList(20);

                        _hexTreeBlueprint.Add(new Point(171, 210), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 210), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(249, 210), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 210), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(317, 210), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(351, 210), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(385, 210), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(164, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 240), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 240), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(300, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(334, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(368, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(402, 240), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(137, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 270), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(249, 270), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(317, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(351, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(385, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(419, 270), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(130, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(164, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(300, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(334, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(368, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(402, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(436, 300), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(103, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(137, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(249, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(317, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(351, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(385, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(419, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(453, 330), HexType.SEA);

                        _hexTreeBlueprint.Add(new Point(130, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(164, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(300, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(334, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(368, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(402, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(436, 360), HexType.ORANGE_SUN);

                        _hexTreeBlueprint.Add(new Point(137, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 390), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(249, 390), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(317, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(351, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(385, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(419, 390), HexType.ORANGE_SUN);

                        _hexTreeBlueprint.Add(new Point(164, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 420), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 420), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(300, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(334, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(368, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(402, 420), HexType.ORANGE_SUN);

                        _hexTreeBlueprint.Add(new Point(171, 450), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 450), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(249, 450), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 450), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(317, 450), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(351, 450), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(385, 450), HexType.ORANGE_SUN);

                        _hexTreeOutcome = new HexList(_hexTreeBlueprint);

                        SeaCount.Value = 0;
                        GreenMoonCount.Value = 0;
                        OrangeSunCount.Value = 0;
                        FieldsCount.Value = 3;
                        PastureCount.Value = 6;
                        MountainsCount.Value = 5;
                        HillsCount.Value = 3;
                        ForestCount.Value = 5;
                        DesertCount.Value = 0;
                        FishCount.Value = 0;
                        CouncilCount.Value = 0;
                        SpiceCount.Value = 0;
                        GoldCount.Value = 0;

                        token2.Value = 1;
                        token3.Value = 2;
                        token4.Value = 3;
                        token5.Value = 2;
                        token6.Value = 3;
                        token8.Value = 3;
                        token9.Value = 2;
                        token10.Value = 3;
                        token11.Value = 2;
                        token12.Value = 1;
                    }
                    break;
                case 9:
                    //E&P 3 Extension
                    {
                            _hexTreeBlueprint = new HexList(20);

                            _hexTreeBlueprint.Add(new Point(171, 210), HexType.EMPTY);
                            _hexTreeBlueprint.Add(new Point(215, 210), HexType.EMPTY);
                            _hexTreeBlueprint.Add(new Point(249, 210), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(283, 210), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(317, 210), HexType.GREEN_MOON);
                            _hexTreeBlueprint.Add(new Point(351, 210), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(385, 210), HexType.GREEN_MOON);
                            _hexTreeBlueprint.Add(new Point(419, 210), HexType.GREEN_MOON);

                            _hexTreeBlueprint.Add(new Point(164, 240), HexType.EMPTY);
                            _hexTreeBlueprint.Add(new Point(198, 240), HexType.EMPTY);
                            _hexTreeBlueprint.Add(new Point(232, 240), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(266, 240), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(300, 240), HexType.GREEN_MOON);
                            _hexTreeBlueprint.Add(new Point(334, 240), HexType.GREEN_MOON);
                            _hexTreeBlueprint.Add(new Point(368, 240), HexType.GREEN_MOON);
                            _hexTreeBlueprint.Add(new Point(402, 240), HexType.GREEN_MOON);
                            _hexTreeBlueprint.Add(new Point(436, 240), HexType.GREEN_MOON);

                            _hexTreeBlueprint.Add(new Point(137, 270), HexType.EMPTY);
                            _hexTreeBlueprint.Add(new Point(171, 270), HexType.EMPTY);
                            _hexTreeBlueprint.Add(new Point(215, 270), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(249, 270), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(283, 270), HexType.GREEN_MOON);
                            _hexTreeBlueprint.Add(new Point(317, 270), HexType.GREEN_MOON);
                            _hexTreeBlueprint.Add(new Point(351, 270), HexType.GREEN_MOON);
                            _hexTreeBlueprint.Add(new Point(385, 270), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(419, 270), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(453, 270), HexType.GREEN_MOON);

                            _hexTreeBlueprint.Add(new Point(130, 300), HexType.EMPTY);
                            _hexTreeBlueprint.Add(new Point(164, 300), HexType.EMPTY);
                            _hexTreeBlueprint.Add(new Point(198, 300), HexType.EMPTY);
                            _hexTreeBlueprint.Add(new Point(232, 300), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(266, 300), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(300, 300), HexType.GREEN_MOON);
                            _hexTreeBlueprint.Add(new Point(334, 300), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(368, 300), HexType.GREEN_MOON);
                            _hexTreeBlueprint.Add(new Point(402, 300), HexType.GREEN_MOON);
                            _hexTreeBlueprint.Add(new Point(436, 300), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(470, 300), HexType.GREEN_MOON);

                            _hexTreeBlueprint.Add(new Point(103, 330), HexType.EMPTY);
                            _hexTreeBlueprint.Add(new Point(137, 330), HexType.EMPTY);
                            _hexTreeBlueprint.Add(new Point(171, 330), HexType.EMPTY);
                            _hexTreeBlueprint.Add(new Point(215, 330), HexType.EMPTY);
                            _hexTreeBlueprint.Add(new Point(249, 330), HexType.COUNCIL);
                            _hexTreeBlueprint.Add(new Point(283, 330), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(317, 330), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(351, 330), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(385, 330), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(419, 330), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(453, 330), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(487, 330), HexType.SEA);

                            _hexTreeBlueprint.Add(new Point(130, 360), HexType.EMPTY);
                            _hexTreeBlueprint.Add(new Point(164, 360), HexType.EMPTY);
                            _hexTreeBlueprint.Add(new Point(198, 360), HexType.EMPTY);
                            _hexTreeBlueprint.Add(new Point(232, 360), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(266, 360), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(300, 360), HexType.ORANGE_SUN);
                            _hexTreeBlueprint.Add(new Point(334, 360), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(368, 360), HexType.ORANGE_SUN);
                            _hexTreeBlueprint.Add(new Point(402, 360), HexType.ORANGE_SUN);
                            _hexTreeBlueprint.Add(new Point(436, 360), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(470, 360), HexType.ORANGE_SUN);

                            _hexTreeBlueprint.Add(new Point(137, 390), HexType.EMPTY);
                            _hexTreeBlueprint.Add(new Point(171, 390), HexType.EMPTY);
                            _hexTreeBlueprint.Add(new Point(215, 390), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(249, 390), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(283, 390), HexType.ORANGE_SUN);
                            _hexTreeBlueprint.Add(new Point(317, 390), HexType.ORANGE_SUN);
                            _hexTreeBlueprint.Add(new Point(351, 390), HexType.ORANGE_SUN);
                            _hexTreeBlueprint.Add(new Point(385, 390), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(419, 390), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(453, 390), HexType.ORANGE_SUN);

                            _hexTreeBlueprint.Add(new Point(164, 420), HexType.EMPTY);
                            _hexTreeBlueprint.Add(new Point(198, 420), HexType.EMPTY);
                            _hexTreeBlueprint.Add(new Point(232, 420), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(266, 420), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(300, 420), HexType.ORANGE_SUN);
                            _hexTreeBlueprint.Add(new Point(334, 420), HexType.ORANGE_SUN);
                            _hexTreeBlueprint.Add(new Point(368, 420), HexType.ORANGE_SUN);
                            _hexTreeBlueprint.Add(new Point(402, 420), HexType.ORANGE_SUN);
                            _hexTreeBlueprint.Add(new Point(436, 420), HexType.ORANGE_SUN);

                            _hexTreeBlueprint.Add(new Point(171, 450), HexType.EMPTY);
                            _hexTreeBlueprint.Add(new Point(215, 450), HexType.EMPTY);
                            _hexTreeBlueprint.Add(new Point(249, 450), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(283, 450), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(317, 450), HexType.ORANGE_SUN);
                            _hexTreeBlueprint.Add(new Point(351, 450), HexType.SEA);
                            _hexTreeBlueprint.Add(new Point(385, 450), HexType.ORANGE_SUN);
                            _hexTreeBlueprint.Add(new Point(419, 450), HexType.ORANGE_SUN);

                            _hexTreeOutcome = new HexList(_hexTreeBlueprint);

                            SeaCount.Value = 0;
                            GreenMoonCount.Value = 0;
                            OrangeSunCount.Value = 0;
                            FieldsCount.Value = 3;
                            PastureCount.Value = 6;
                            MountainsCount.Value = 5;
                            HillsCount.Value = 3;
                            ForestCount.Value = 5;
                            DesertCount.Value = 0;
                            FishCount.Value = 0;
                            CouncilCount.Value = 0;
                            SpiceCount.Value = 0;
                            GoldCount.Value = 0;

                            token2.Value = 1;
                            token3.Value = 2;
                            token4.Value = 3;
                            token5.Value = 2;
                            token6.Value = 3;
                            token8.Value = 3;
                            token9.Value = 2;
                            token10.Value = 3;
                            token11.Value = 2;
                            token12.Value = 1;
                        }
                    break;
                case 10:
                    //E&P 4 Extension
                    {
                        _hexTreeBlueprint = new HexList(20);

                        _hexTreeBlueprint.Add(new Point(171, 210), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 210), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(249, 210), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 210), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(317, 210), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(351, 210), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(385, 210), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(419, 210), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(164, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 240), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 240), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(300, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(334, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(368, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(402, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(436, 240), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(137, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 270), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(249, 270), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(317, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(351, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(385, 270), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(419, 270), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(453, 270), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(130, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(164, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(300, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(334, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(368, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(402, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(436, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(470, 300), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(103, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(137, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(249, 330), HexType.COUNCIL);
                        _hexTreeBlueprint.Add(new Point(283, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(317, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(351, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(385, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(419, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(453, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(487, 330), HexType.SEA);

                        _hexTreeBlueprint.Add(new Point(130, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(164, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(300, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(334, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(368, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(402, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(436, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(470, 360), HexType.ORANGE_SUN);

                        _hexTreeBlueprint.Add(new Point(137, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 390), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(249, 390), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(317, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(351, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(385, 390), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(419, 390), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(453, 390), HexType.ORANGE_SUN);

                        _hexTreeBlueprint.Add(new Point(164, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 420), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 420), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(300, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(334, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(368, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(402, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(436, 420), HexType.ORANGE_SUN);

                        _hexTreeBlueprint.Add(new Point(171, 450), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 450), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(249, 450), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 450), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(317, 450), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(351, 450), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(385, 450), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(419, 450), HexType.ORANGE_SUN);

                        _hexTreeOutcome = new HexList(_hexTreeBlueprint);

                        SeaCount.Value = 0;
                        GreenMoonCount.Value = 0;
                        OrangeSunCount.Value = 0;
                        FieldsCount.Value = 3;
                        PastureCount.Value = 6;
                        MountainsCount.Value = 5;
                        HillsCount.Value = 3;
                        ForestCount.Value = 5;
                        DesertCount.Value = 0;
                        FishCount.Value = 0;
                        CouncilCount.Value = 0;
                        SpiceCount.Value = 0;
                        GoldCount.Value = 0;

                        token2.Value = 1;
                        token3.Value = 2;
                        token4.Value = 3;
                        token5.Value = 2;
                        token6.Value = 3;
                        token8.Value = 3;
                        token9.Value = 2;
                        token10.Value = 3;
                        token11.Value = 2;
                        token12.Value = 1;
                    }
                    break;
                case 11:
                    //E&P 5 Extension
                    {
                        _hexTreeBlueprint = new HexList(20);

                        _hexTreeBlueprint.Add(new Point(171, 210), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 210), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(249, 210), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 210), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(317, 210), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(351, 210), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(385, 210), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(419, 210), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(453, 210), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(164, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 240), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 240), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(300, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(334, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(368, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(402, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(436, 240), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(470, 240), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(137, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 270), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(249, 270), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(317, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(351, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(385, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(419, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(453, 270), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(487, 270), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(130, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(164, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(300, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(334, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(368, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(402, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(436, 300), HexType.GREEN_MOON);
                        _hexTreeBlueprint.Add(new Point(470, 300), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(504, 300), HexType.GREEN_MOON);

                        _hexTreeBlueprint.Add(new Point(103, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(137, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(249, 330), HexType.COUNCIL);
                        _hexTreeBlueprint.Add(new Point(283, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(317, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(351, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(385, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(419, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(453, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(487, 330), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(521, 330), HexType.SEA);

                        _hexTreeBlueprint.Add(new Point(130, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(164, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(300, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(334, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(368, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(402, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(436, 360), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(470, 360), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(504, 360), HexType.ORANGE_SUN);

                        _hexTreeBlueprint.Add(new Point(137, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 390), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(249, 390), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(317, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(351, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(385, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(419, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(453, 390), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(487, 390), HexType.ORANGE_SUN);

                        _hexTreeBlueprint.Add(new Point(164, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 420), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 420), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(266, 420), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(300, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(334, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(368, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(402, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(436, 420), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(470, 420), HexType.ORANGE_SUN);

                        _hexTreeBlueprint.Add(new Point(171, 450), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 450), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(249, 450), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(283, 450), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(317, 450), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(351, 450), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(385, 450), HexType.ORANGE_SUN);
                        _hexTreeBlueprint.Add(new Point(419, 450), HexType.SEA);
                        _hexTreeBlueprint.Add(new Point(453, 450), HexType.ORANGE_SUN);

                        _hexTreeOutcome = new HexList(_hexTreeBlueprint);

                        SeaCount.Value = 0;
                        GreenMoonCount.Value = 0;
                        OrangeSunCount.Value = 0;
                        FieldsCount.Value = 3;
                        PastureCount.Value = 6;
                        MountainsCount.Value = 5;
                        HillsCount.Value = 3;
                        ForestCount.Value = 5;
                        DesertCount.Value = 0;
                        FishCount.Value = 0;
                        CouncilCount.Value = 0;
                        SpiceCount.Value = 0;
                        GoldCount.Value = 0;

                        token2.Value = 1;
                        token3.Value = 2;
                        token4.Value = 3;
                        token5.Value = 2;
                        token6.Value = 3;
                        token8.Value = 3;
                        token9.Value = 2;
                        token10.Value = 3;
                        token11.Value = 2;
                        token12.Value = 1;
                    }
                    break;
                case 12:
                    //48
                    {
                        _hexTreeBlueprint = new HexList(20);

                        _hexTreeBlueprint.Add(new Point(198, 180), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 180), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(266, 180), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(300, 180), HexType.EMPTY);

                        _hexTreeBlueprint.Add(new Point(171, 210), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 210), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(249, 210), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(283, 210), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(317, 210), HexType.EMPTY);

                        _hexTreeBlueprint.Add(new Point(164, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(266, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(300, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(334, 240), HexType.EMPTY);

                        _hexTreeBlueprint.Add(new Point(137, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(249, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(283, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(317, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(351, 270), HexType.EMPTY);

                        _hexTreeBlueprint.Add(new Point(130, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(164, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(266, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(300, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(334, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(368, 300), HexType.EMPTY);
                        
                        _hexTreeBlueprint.Add(new Point(137, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(249, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(283, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(317, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(351, 330), HexType.EMPTY);
                        
                        _hexTreeBlueprint.Add(new Point(164, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(266, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(300, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(334, 360), HexType.EMPTY);
                        
                        _hexTreeBlueprint.Add(new Point(171, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(249, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(283, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(317, 390), HexType.EMPTY);

                        _hexTreeOutcome = new HexList(_hexTreeBlueprint);

                        SeaCount.Value = 36;
                        GreenMoonCount.Value = 0;
                        OrangeSunCount.Value = 0;
                        FieldsCount.Value = 10;
                        PastureCount.Value = 9;
                        MountainsCount.Value = 10;
                        HillsCount.Value = 8;
                        ForestCount.Value = 9;
                        DesertCount.Value = 2;
                        FishCount.Value = 6;
                        CouncilCount.Value = 2;
                        SpiceCount.Value = 6;
                        GoldCount.Value = 8;

                        token2.Value = 4;
                        token3.Value = 7;
                        token4.Value = 8;
                        token5.Value = 8;
                        token6.Value = 6;
                        token8.Value = 6;
                        token9.Value = 8;
                        token10.Value = 8;
                        token11.Value = 6;
                        token12.Value = 3;
                    }
                    break;
                case 13:
                    //37
                    {
                        _hexTreeBlueprint = new HexList(20);
                        

                        _hexTreeBlueprint.Add(new Point(171, 210), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 210), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(249, 210), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(283, 210), HexType.EMPTY);
                        
                        _hexTreeBlueprint.Add(new Point(164, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(266, 240), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(300, 240), HexType.EMPTY);
                        
                        _hexTreeBlueprint.Add(new Point(137, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(249, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(283, 270), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(317, 270), HexType.EMPTY);
                        
                        _hexTreeBlueprint.Add(new Point(130, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(164, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(266, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(300, 300), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(334, 300), HexType.EMPTY);
                        
                        _hexTreeBlueprint.Add(new Point(137, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(171, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(249, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(283, 330), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(317, 330), HexType.EMPTY);
                        
                        _hexTreeBlueprint.Add(new Point(164, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(198, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(232, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(266, 360), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(300, 360), HexType.EMPTY);
                        
                        _hexTreeBlueprint.Add(new Point(171, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(215, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(249, 390), HexType.EMPTY);
                        _hexTreeBlueprint.Add(new Point(283, 390), HexType.EMPTY);
                        
                        _hexTreeOutcome = new HexList(_hexTreeBlueprint);

                        SeaCount.Value = 36;
                        GreenMoonCount.Value = 0;
                        OrangeSunCount.Value = 0;
                        FieldsCount.Value = 10;
                        PastureCount.Value = 9;
                        MountainsCount.Value = 10;
                        HillsCount.Value = 8;
                        ForestCount.Value = 9;
                        DesertCount.Value = 2;
                        FishCount.Value = 6;
                        CouncilCount.Value = 2;
                        SpiceCount.Value = 6;
                        GoldCount.Value = 8;

                        token2.Value = 4;
                        token3.Value = 7;
                        token4.Value = 8;
                        token5.Value = 8;
                        token6.Value = 6;
                        token8.Value = 6;
                        token9.Value = 8;
                        token10.Value = 8;
                        token11.Value = 6;
                        token12.Value = 3;
                    }
                    break;
            }
            pictureBox1.Refresh();
            pictureBox2.Refresh();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;

namespace CatanBoardMaker
{
    class HexList : IEnumerable<Hex>
    {
        private List<Hex> _hexes;
        private float _radius;

        public HexList(float radius)
        {
            _hexes = new List<Hex>();
            _radius = radius;
        }
        public void Draw(Graphics graphics)
        {
            foreach (Hex hex in _hexes)
            {
                hex.Draw(graphics);
            }
        }
        public void Add(Point point,HexType type)
        {
            if (_hexes.Count == 0)
            {
                _hexes.Add(new Hex(point, _radius, type));
            }
            else
            {
                Hex newHex=new Hex(new Point(0,0),0,HexType.EMPTY);
                foreach (Hex hex in _hexes)
                {
                    if (hex.PointIsInEast(point))
                    {
                        if (hex.EastHex != null)
                        {
                            MessageBox.Show("There is already a hex there!");
                            return;
                        }
                        newHex = new Hex(hex.EastHexCentre, _radius, type);
                        newHex.WestHex = hex;
                        hex.EastHex = newHex;
                        foreach (Hex deepHex in _hexes)
                        {
                            if (deepHex.ContainsPoint(newHex.NorthWestHexCentre))
                            {
                                newHex.NorthWestHex = deepHex;
                                deepHex.SouthEastHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.NorthEastHexCentre))
                            {
                                newHex.NorthEastHex = deepHex;
                                deepHex.SouthWestHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.EastHexCentre))
                            {
                                newHex.EastHex = deepHex;
                                deepHex.WestHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.SouthEastHexCentre))
                            {
                                newHex.SouthEastHex = deepHex;
                                deepHex.NorthWestHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.SouthWestHexCentre))
                            {
                                newHex.SouthWestHex = deepHex;
                                deepHex.NorthEastHex = newHex;
                            }
                        }
                        break;
                    }

                    else if (hex.PointIsInNorthEast(point))
                    {
                        if (hex.NorthEastHex != null)
                        {
                            MessageBox.Show("There is already a hex there!");
                            return;
                        }
                        newHex = new Hex(hex.NorthEastHexCentre, _radius, type);
                        newHex.SouthWestHex = hex;
                        hex.NorthEastHex = newHex;
                        foreach (Hex deepHex in _hexes)
                        {
                            if (deepHex.ContainsPoint(newHex.NorthWestHexCentre))
                            {
                                newHex.NorthWestHex = deepHex;
                                deepHex.SouthEastHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.NorthEastHexCentre))
                            {
                                newHex.NorthEastHex = deepHex;
                                deepHex.SouthWestHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.EastHexCentre))
                            {
                                newHex.EastHex = deepHex;
                                deepHex.WestHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.SouthEastHexCentre))
                            {
                                newHex.SouthEastHex = deepHex;
                                deepHex.NorthWestHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.WestHexCentre))
                            {
                                newHex.WestHex = deepHex;
                                deepHex.EastHex = newHex;
                            }
                        }
                        break;
                    }
                    else if (hex.PointIsInNorthWest(point))
                    {
                        if (hex.NorthWestHex != null)
                        {
                            MessageBox.Show("There is already a hex there!");
                            return;
                        }
                        newHex = new Hex(hex.NorthWestHexCentre, _radius, type);
                        newHex.SouthEastHex = hex;
                        hex.NorthWestHex = newHex;
                        foreach (Hex deepHex in _hexes)
                        {
                            if (deepHex.ContainsPoint(newHex.NorthWestHexCentre))
                            {
                                newHex.NorthWestHex = deepHex;
                                deepHex.SouthEastHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.NorthEastHexCentre))
                            {
                                newHex.NorthEastHex = deepHex;
                                deepHex.SouthWestHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.EastHexCentre))
                            {
                                newHex.EastHex = deepHex;
                                deepHex.WestHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.WestHexCentre))
                            {
                                newHex.WestHex = deepHex;
                                deepHex.EastHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.SouthWestHexCentre))
                            {
                                newHex.SouthWestHex = deepHex;
                                deepHex.NorthEastHex = newHex;
                            }
                        }
                        break;
                    }
                    else if (hex.PointIsInSouthEast(point))
                    {
                        if (hex.SouthEastHex != null)
                        {
                            MessageBox.Show("There is already a hex there!");
                            return;
                        }
                        newHex = new Hex(hex.SouthEastHexCentre, _radius, type);
                        newHex.NorthWestHex = hex;
                        hex.SouthEastHex = newHex;
                        foreach (Hex deepHex in _hexes)
                        {
                            if (deepHex.ContainsPoint(newHex.WestHexCentre))
                            {
                                newHex.WestHex = deepHex;
                                deepHex.EastHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.NorthEastHexCentre))
                            {
                                newHex.NorthEastHex = deepHex;
                                deepHex.SouthWestHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.EastHexCentre))
                            {
                                newHex.EastHex = deepHex;
                                deepHex.WestHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.SouthEastHexCentre))
                            {
                                newHex.SouthEastHex = deepHex;
                                deepHex.NorthWestHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.SouthWestHexCentre))
                            {
                                newHex.SouthWestHex = deepHex;
                                deepHex.NorthEastHex = newHex;
                            }
                        }
                        break;
                    }
                    else if (hex.PointIsInSouthWest(point))
                    {
                        if (hex.SouthWestHex != null)
                        {
                            MessageBox.Show("There is already a hex there!");
                            return;
                        }
                        newHex = new Hex(hex.SouthWestHexCentre, _radius, type);
                        newHex.NorthEastHex = hex;
                        hex.SouthWestHex = newHex;
                        foreach (Hex deepHex in _hexes)
                        {
                            if (deepHex.ContainsPoint(newHex.NorthWestHexCentre))
                            {
                                newHex.NorthWestHex = deepHex;
                                deepHex.SouthEastHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.WestHexCentre))
                            {
                                newHex.WestHex = deepHex;
                                deepHex.EastHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.EastHexCentre))
                            {
                                newHex.EastHex = deepHex;
                                deepHex.WestHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.SouthEastHexCentre))
                            {
                                newHex.SouthEastHex = deepHex;
                                deepHex.NorthWestHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.SouthWestHexCentre))
                            {
                                newHex.SouthWestHex = deepHex;
                                deepHex.NorthEastHex = newHex;
                            }
                        }
                        break;
                    }
                    else if (hex.PointIsInWest(point))
                    {
                        if (hex.WestHex != null)
                        {
                            MessageBox.Show("There is already a hex there!");
                            return;
                        }
                        newHex = new Hex(hex.WestHexCentre, _radius, type);
                        newHex.EastHex = hex;
                        hex.WestHex = newHex;
                        foreach (Hex deepHex in _hexes)
                        {
                            if (deepHex.ContainsPoint(newHex.NorthWestHexCentre))
                            {
                                newHex.NorthWestHex = deepHex;
                                deepHex.SouthEastHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.NorthEastHexCentre))
                            {
                                newHex.NorthEastHex = deepHex;
                                deepHex.SouthWestHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.WestHexCentre))
                            {
                                newHex.WestHex = deepHex;
                                deepHex.EastHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.SouthEastHexCentre))
                            {
                                newHex.SouthEastHex = deepHex;
                                deepHex.NorthWestHex = newHex;
                            }
                            else if (deepHex.ContainsPoint(newHex.SouthWestHexCentre))
                            {
                                newHex.SouthWestHex = deepHex;
                                deepHex.NorthEastHex = newHex;
                            }
                        }
                        break;
                    }
                }
                _hexes.Add(newHex);
            }
        }
        public void Move(int changeX, int changeY)
        {
            foreach (Hex hex in _hexes)
            {
                hex.Move(changeX, changeY);
            }
        }
        public Hex Find(Point point)
        {
            foreach (Hex hex in _hexes)
            {
                if (hex.ContainsPoint(point))
                {
                    return hex;
                }
            }
            return null;
        }
        public int Count(HexType type)
        {
            int total = 0;
            foreach (Hex hex in _hexes)
            {
                if (hex.Type == type)
                {
                    total++;
                }
            }
            return total;
        }
        public int Count()
        {
            return _hexes.Count;
        }
        public Hex GetFirst(HexType type)
        {
            foreach (Hex hex in _hexes)
            {
                if (hex.Type == type)
                {
                    return hex;
                }
            }
            return null;
        }
        public HexList(HexList list)
        {
            _hexes = new List<Hex>();
            _radius = list._radius;
            foreach (Hex hex in list._hexes)
            {
                Add(hex.Centre, hex.Type);
            }
        }
        public int NumUntokened
        {
            get
            {
                int total = 0;
                foreach (Hex hex in _hexes)
                {
                    if (hex.TokenNumber==0&&hex.NeedsToken)
                    {
                        total++;
                    }
                }
                return total;
            }
        }
        public Hex FirstUntokened
        {
            get
            {
                foreach (Hex hex in _hexes)
                {
                    if (hex.TokenNumber == 0&&hex.NeedsToken)
                    {
                        return hex;
                    }
                }
                return null;
            }
        }
        public void ResetNumbers()
        {
            foreach(Hex hex in _hexes)
            {
                if (hex.NeedsToken)
                {
                    hex.TokenNumber = 0;
                }
            }
        }

        public IEnumerator<Hex> GetEnumerator()
        {
            foreach (Hex hex in _hexes)
            {
                yield return hex;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}

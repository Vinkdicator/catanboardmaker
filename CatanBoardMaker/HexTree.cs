﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace CatanBoardMaker
{/*
    class HexTree
    {


        public void AddEast(HexType type)
        {
            if (_eastBranch == null)
            {
                _eastBranch = new HexTree(type, new Point(_trunk.Centre.X + (_trunk.NorthEastPoint.X - _trunk.Centre.X) * 2, _trunk.Centre.Y), _trunk.Radius);
                _eastIsChild = true;
            }
            else
            {
                MessageBox.Show("There's a hex in the way!");
            }
        }
        public void AddWest(HexType type)
        {
            if (_westBranch == null)
            {
                _westBranch = new HexTree(type, new Point(_trunk.Centre.X - (_trunk.NorthEastPoint.X - _trunk.Centre.X) * 2, _trunk.Centre.Y), _trunk.Radius);
                _westIsChild = true;
            }
            else
            {
                MessageBox.Show("There's a hex in the way!");
            }
        }
        public void AddNorthEast(HexType type)
        {
            if (_northEastBranch == null)
            {
                _northEastBranch = new HexTree(type, new Point(_trunk.Centre.X + (_trunk.NorthEastPoint.X - _trunk.Centre.X), _trunk.Centre.Y + (int)((_trunk.NorthPoint.Y - _trunk.Centre.Y) * 1.5)), _trunk.Radius);
                _northEastIsChild = true;
            }
            else
            {
                MessageBox.Show("There's a hex in the way!");
            }
        }
        public void AddNorthWest(HexType type)
        {
            if (_northWestBranch == null)
            {
                _northWestBranch = new HexTree(type, new Point(_trunk.Centre.X - (_trunk.NorthEastPoint.X - _trunk.Centre.X), _trunk.Centre.Y + (int)((_trunk.NorthPoint.Y - _trunk.Centre.Y) * 1.5)), _trunk.Radius);
                _northWestIsChild = true;
            }
            else
            {
                MessageBox.Show("There's a hex in the way!");
            }
        }
        public void AddSouthEast(HexType type)
        {
            if (_southEastBranch == null)
            {
                _northEastBranch = new HexTree(type, new Point(_trunk.Centre.X + (_trunk.NorthEastPoint.X - _trunk.Centre.X), _trunk.Centre.Y - (int)((_trunk.NorthPoint.Y - _trunk.Centre.Y) * 1.5)), _trunk.Radius);
                _northEastIsChild = true;
            }
            else
            {
                MessageBox.Show("There's a hex in the way!");
            }
        }
        public void AddSouthWest(HexType type)
        {
            if (_southWestBranch == null)
            {
                _northWestBranch = new HexTree(type, new Point(_trunk.Centre.X - (_trunk.NorthEastPoint.X - _trunk.Centre.X), _trunk.Centre.Y - (int)((_trunk.NorthPoint.Y - _trunk.Centre.Y) * 1.5)), _trunk.Radius);
                _northWestIsChild = true;
            }
            else
            {
                MessageBox.Show("There's a hex in the way!");
            }
        }

        private bool PointIsInEast(Point point)
        {
            if (_eastBranch != null)
            {
                MessageBox.Show("A hex was already known in the east.");
                return true;
            }
            Hex fakeHex = new Hex(new Point(_trunk.Centre.X + (_trunk.NorthEastPoint.X - _trunk.Centre.X) * 2, _trunk.Centre.Y), _trunk.Radius, HexType.EMPTY);
            return fakeHex.ContainsPoint(point);
        }
        private bool PointIsInWest(Point point)
        {
            if (_westBranch != null)
            {
                MessageBox.Show("A hex was already known in the west.");
                return true;
            }
            Hex fakeHex = new Hex(new Point(_trunk.Centre.X - (_trunk.NorthEastPoint.X - _trunk.Centre.X) * 2, _trunk.Centre.Y), _trunk.Radius, HexType.EMPTY);
            return fakeHex.ContainsPoint(point);
        }
        private bool PointIsInNorthWest(Point point)
        {
            if (_westBranch != null)
            {
                MessageBox.Show("A hex was already known in the northwest.");
                return true;
            }
            Hex fakeHex = new Hex(new Point(_trunk.Centre.X - (_trunk.NorthEastPoint.X - _trunk.Centre.X), _trunk.Centre.Y + (int)((_trunk.NorthPoint.Y - _trunk.Centre.Y) * 1.5)), _trunk.Radius, HexType.EMPTY);
            return fakeHex.ContainsPoint(point);
        }
        private bool PointIsInNorthEast(Point point)
        {
            if (_westBranch != null)
            {
                MessageBox.Show("A hex was already known in the northeast.");
                return true;
            }
            Hex fakeHex = new Hex(new Point(_trunk.Centre.X + (_trunk.NorthEastPoint.X - _trunk.Centre.X), _trunk.Centre.Y + (int)((_trunk.NorthPoint.Y - _trunk.Centre.Y) * 1.5)), _trunk.Radius, HexType.EMPTY);
            return fakeHex.ContainsPoint(point);
        }
        private bool PointIsInSouthWest(Point point)
        {
            if (_westBranch != null)
            {
                MessageBox.Show("A hex was already known in the southwest.");
                return true;
            }
            Hex fakeHex = new Hex(new Point(_trunk.Centre.X - (_trunk.NorthEastPoint.X - _trunk.Centre.X), _trunk.Centre.Y - (int)((_trunk.NorthPoint.Y - _trunk.Centre.Y) * 1.5)), _trunk.Radius, HexType.EMPTY);
            return fakeHex.ContainsPoint(point);
        }
        private bool PointIsInSouthEast(Point point)
        {
            if (_westBranch != null)
            {
                MessageBox.Show("A hex was already known in the southeast.");
                return true;
            }
            Hex fakeHex = new Hex(new Point(_trunk.Centre.X + (_trunk.NorthEastPoint.X - _trunk.Centre.X), _trunk.Centre.Y - (int)((_trunk.NorthPoint.Y - _trunk.Centre.Y) * 1.5)), _trunk.Radius, HexType.EMPTY);
            return fakeHex.ContainsPoint(point);
        }
        
        /// <summary>
        /// Returns a hex tree where the trunk contains the specified point. If none is found, null is returned.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public HexTree Find(Point point)
        {
            if (_trunk.ContainsPoint(point))
            {
                return this;
            }
            else
            {
                float distFromC = _trunk.DistanceFromCentre(point);
                float minDist = distFromC;
                HexTree closestTree=this;
                for (int i = 0; i < 6; i++)
                {
                    if (_trees[i] != null)
                    {
                        //MessageBox.Show("Not all the trees were null");
                        float thisTreeDist = _trees[i].Trunk.DistanceFromCentre(point);
                        if (thisTreeDist < minDist)
                        {
                            minDist = thisTreeDist;
                            closestTree = _trees[i];
                        }
                    }
                }
                if (minDist < distFromC)
                {
                    return closestTree.Find(point);
                }
                else
                {
                    return null;
                }
            }
        }
        public void Draw(Graphics graphics)
        {
            _trunk.Draw(graphics);
            if (_eastIsChild)
            {
                _eastBranch.Draw(graphics);
            }
            if (_westIsChild)
            {
                _westBranch.Draw(graphics);
            }
            if (_northEastIsChild)
            {
                _northEastBranch.Draw(graphics);
            }
            if (_northWestIsChild)
            {
                _northWestBranch.Draw(graphics);
            }
            if (_southEastIsChild)
            {
                _southEastBranch.Draw(graphics);
            }
            if (_southWestIsChild)
            {
                _southWestBranch.Draw(graphics);
            }
        }
        public HexTree(HexType type,Point centre,float radius)
        {
            _trunk = new Hex(centre, radius, type);
            _trees = new HexTree[6];
        }
    }*/
}

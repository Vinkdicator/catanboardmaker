﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace CatanBoardMaker
{
    public enum HexType { FOREST,HILLS,MOUNTAINS,PASTURE,FIELDS,FISH,SPICE,GOLD,DESERT,COUNCIL,SEA,GREEN_MOON,ORANGE_SUN,EMPTY}
    public class Hex
    {
        private Hex[] _neighbours;
        private HexType _type;
        private Point[] _points;
        private bool _needsToken;
        private int _tokenNumber;
        //0 is northeast
        //1 is north
        //2 is northwest
        //3 is southwest
        //4 is south
        //5 is southeast
        private Point _centre;
        private float _radius;

        private Point[] _hexCentres;
        private Point _eastHexCentre { get => _hexCentres[0]; set => _hexCentres[0] = value; }
        private Point _westHexCentre { get => _hexCentres[1]; set => _hexCentres[1] = value; }
        private Point _northEastHexCentre { get => _hexCentres[2]; set => _hexCentres[2] = value; }
        private Point _northWestHexCentre { get => _hexCentres[3]; set => _hexCentres[3] = value; }
        private Point _southEastHexCentre { get => _hexCentres[4]; set => _hexCentres[4] = value; }
        private Point _southWestHexCentre { get => _hexCentres[5]; set => _hexCentres[5] = value; }

        public Point EastHexCentre { get => _hexCentres[0]; }
        public Point WestHexCentre { get => _hexCentres[1];  }
        public Point NorthEastHexCentre { get => _hexCentres[2]; }
        public Point NorthWestHexCentre { get => _hexCentres[3];}
        public Point SouthEastHexCentre { get => _hexCentres[4];}
        public Point SouthWestHexCentre { get => _hexCentres[5]; }
       
        public Hex EastHex { get => _neighbours[0]; set => _neighbours[0] = value; }
        public Hex WestHex { get => _neighbours[1]; set => _neighbours[1] = value; }
        public Hex NorthWestHex { get => _neighbours[2]; set => _neighbours[2] = value; }
        public Hex NorthEastHex { get => _neighbours[3]; set => _neighbours[3] = value; }
        public Hex SouthEastHex { get => _neighbours[4]; set => _neighbours[4] = value; }
        public Hex SouthWestHex { get => _neighbours[5]; set => _neighbours[5] = value; }

        protected Brush _brush;

        public HexType Type
        {
            get => _type;
            set
            {
                _type = value;
                AssignTypeDependencies();
            }
        }

        public Point Centre { get => _centre; }
        public float Radius { get => _radius; }
        public Point NorthEastPoint
        {
            get
            {
                return _points[0];
            }
        }
        public Point NorthPoint
        {
            get
            {
                return _points[1];
            }
        }
        public Point NorthWestPoint
        {
            get
            {
                return _points[2];
            }
        }
        public Point SouthWestPoint
        {
            get
            {
                return _points[3];
            }
        }
        public Point SouthPoint
        {
            get
            {
                return _points[4];
            }
        }
        public Point SouthEastPoint
        {
            get
            {
                return _points[5];
            }
        }

        public int TokenNumber { get => _tokenNumber; set => _tokenNumber = value; }

        public bool PointIsInEast(Point point)
        {
            Hex fakeHex = new Hex(_eastHexCentre, _radius, HexType.EMPTY);
            return fakeHex.ContainsPoint(point);
        }
        public bool PointIsInWest(Point point)
        {
            Hex fakeHex = new Hex(_westHexCentre, _radius, HexType.EMPTY);
            return fakeHex.ContainsPoint(point);
        }
        public bool PointIsInNorthWest(Point point)
        {
            Hex fakeHex = new Hex(_northWestHexCentre, _radius, HexType.EMPTY);
            return fakeHex.ContainsPoint(point);
        }
        public bool PointIsInNorthEast(Point point)
        {
            Hex fakeHex = new Hex(_northEastHexCentre, _radius, HexType.EMPTY);
            return fakeHex.ContainsPoint(point);
        }
        public bool PointIsInSouthWest(Point point)
        {
            Hex fakeHex = new Hex(_southWestHexCentre, _radius, HexType.EMPTY);
            return fakeHex.ContainsPoint(point);
        }
        public bool PointIsInSouthEast(Point point)
        {
            Hex fakeHex = new Hex(_southEastHexCentre, _radius, HexType.EMPTY);
            return fakeHex.ContainsPoint(point);
        }

        public bool CanHoldRedToken
        {
            get
            {
                return ((NorthEastHex==null|| !NorthEastHex.NeedsToken || NorthEastHex.TokenNumber!=6&& NorthEastHex.TokenNumber != 8)&& (NorthWestHex == null || !NorthWestHex.NeedsToken || NorthWestHex.TokenNumber != 6 && NorthWestHex.TokenNumber != 8)&& (EastHex == null || !EastHex.NeedsToken || EastHex.TokenNumber != 6 && EastHex.TokenNumber != 8)&& (WestHex == null || !WestHex.NeedsToken || WestHex.TokenNumber != 6 && WestHex.TokenNumber != 8)&& (SouthEastHex == null || !SouthEastHex.NeedsToken || SouthEastHex.TokenNumber != 6 && SouthEastHex.TokenNumber != 8)&& (SouthWestHex == null || !SouthWestHex.NeedsToken || SouthWestHex.TokenNumber != 6 && SouthWestHex.TokenNumber != 8));
            }
        }

        public bool NeedsToken { get => _needsToken; }

        public bool Neighbours(Point point)
        {
            return (PointIsInEast(point)||PointIsInNorthEast(point)||PointIsInNorthWest(point)||PointIsInSouthEast(point)||PointIsInSouthWest(point)||PointIsInWest(point));
        }

        public void Move(int xChange, int yChange)
        {
            _centre.X += xChange;
            _centre.Y += yChange;
            ConfigurePoints();
            ConfigureCentres();
        }

        public void Draw(Graphics graphics)
        {
            if (_type == HexType.EMPTY)
            {
                graphics.DrawPolygon(Pens.Black, _points);
            }
            else
            {
                graphics.FillPolygon(_brush, _points);
                if (_needsToken&&(NorthEastPoint.X!=NorthWestPoint.X))
                {
                    int circleRadius = (NorthEastPoint.X-NorthWestPoint.X)/6;
                    SizeF tokenTextSizeF = new SizeF(circleRadius*1.5f,1000);
                    SizeF currentSize = graphics.MeasureString("12", new Font("Arial", 40), tokenTextSizeF);
                    int fontSize = 40;
                    while (currentSize.Width > circleRadius * 4f || currentSize.Height > circleRadius * 4f&&fontSize>0)
                    {
                        fontSize--;
                        currentSize = graphics.MeasureString("12", new Font("Arial", fontSize), tokenTextSizeF);
                    }
                    //int spareSpace = (int)(circleRadius * 1.5f) - (int)currentSize.Height;
                    graphics.FillEllipse(Brushes.White, _centre.X - circleRadius, _centre.Y - circleRadius, circleRadius * 2, circleRadius * 2);
                    if (_tokenNumber!=6&&_tokenNumber!=8)
                    graphics.DrawString(_tokenNumber.ToString(), new Font("Arial", fontSize), Brushes.Black, new Rectangle(_centre.X-circleRadius, _centre.Y -circleRadius, circleRadius*3,circleRadius*3));
                    else
                    {
                        graphics.DrawString(_tokenNumber.ToString(), new Font("Arial", fontSize), Brushes.Red, new Rectangle(_centre.X - circleRadius, _centre.Y - circleRadius, circleRadius * 3, circleRadius * 3));
                    }
                }
                else if (_needsToken)
                {
                    MessageBox.Show("a strange hex is here");
                }
            }
        }
        private void ConfigureCentres()
        {
            _eastHexCentre = new Point(_centre.X + (NorthEastPoint.X - _centre.X) * 2, _centre.Y);
            _westHexCentre = new Point(_centre.X - (NorthEastPoint.X - _centre.X) * 2, _centre.Y);
            _northWestHexCentre = new Point(_centre.X - (NorthEastPoint.X - _centre.X), _centre.Y + (int)((NorthPoint.Y - _centre.Y) * 1.5));
            _northEastHexCentre = new Point(_centre.X + (NorthEastPoint.X - _centre.X), _centre.Y + (int)((NorthPoint.Y - _centre.Y) * 1.5));
            _southWestHexCentre = new Point(_centre.X - (NorthEastPoint.X - _centre.X), _centre.Y - (int)((NorthPoint.Y - _centre.Y) * 1.5));
            _southEastHexCentre = new Point(_centre.X + (NorthEastPoint.X - _centre.X), _centre.Y - (int)((NorthPoint.Y - _centre.Y) * 1.5));
        }
        public Hex(Point centre, float radius,HexType type)
        {
            _centre = centre;
            _radius = radius;
            _points = new Point[6];
            ConfigurePoints();
            _type = type;
            _neighbours = new Hex[6];
            _hexCentres = new Point[6];
            ConfigureCentres();
            AssignTypeDependencies();
        }
        private void AssignTypeDependencies()
        {
            switch (_type)
            {
                case HexType.DESERT:
                    _brush = new SolidBrush(Color.FromArgb(255, 242, 206, 75));
                    _needsToken = false;
                    break;
                case HexType.FIELDS:
                    _brush = new SolidBrush(Color.FromArgb(255, 255, 246, 7));
                    _needsToken = true;
                    break;
                case HexType.FISH:
                    _brush = new SolidBrush(Color.Purple);
                    _needsToken = false;
                    break;
                case HexType.COUNCIL:
                    _brush = new SolidBrush(Color.LightBlue);
                    _needsToken = false;
                    break;
                case HexType.FOREST:
                    _brush = new SolidBrush(Color.DarkGreen);
                    _needsToken = true;
                    break;
                case HexType.PASTURE:
                    _needsToken = true;
                    _brush = new SolidBrush(Color.LightGreen);
                    break;
                case HexType.MOUNTAINS:
                    _needsToken = true;
                    _brush = new SolidBrush(Color.LightGray);
                    break;
                case HexType.HILLS:
                    _needsToken = true;
                    _brush = new SolidBrush(Color.Orange);
                    break;
                case HexType.SEA:
                    _needsToken = false;
                    _brush = new SolidBrush(Color.FromArgb(255, 0, 0, 255));
                    break;
                case HexType.GREEN_MOON:
                    _needsToken = false;
                    _brush = new SolidBrush(Color.Aqua);
                    break;
                case HexType.ORANGE_SUN:
                    _needsToken = false;
                    _brush = new SolidBrush(Color.DarkOrange);
                    break;
                case HexType.GOLD:
                    _needsToken = false;
                    _brush = new SolidBrush(Color.Gray);
                    break;
                case HexType.SPICE:
                    _needsToken = false;
                    _brush = new SolidBrush(Color.Green);
                    break;
            }
            _tokenNumber = 0;
        }

        private void ConfigurePoints()
        {
            for (int a = 0; a < 6; a++)
            {
                _points[a] = new Point(
                    (int)(_centre.X + _radius * (float)Math.Cos(a * 60 * Math.PI / 180f + 30 * Math.PI / 180f)),
                    (int)(_centre.Y + _radius * (float)Math.Sin(a * 60 * Math.PI / 180f + 30 * Math.PI / 180f)));
            }
        }

        public bool ContainsPoint(Point point)
        {
            //if x is greater than an east point's x
            if (point.X > _points[0].X)
            {
                return false;
            }
            //if x is less than a west point's x
            else if (point.X < _points[3].X)
            {
                return false;
            }
            //if y is greater than the north point's y
            else if (point.Y > _points[1].Y)
            {
                return false;
            }
            //if y is less than the south point's y
            else if (point.Y < _points[4].Y)
            {
                return false;
            }
            //north-northeast line check
            float gradient = ((float)(_points[1].Y-_points[0].Y))/((float)(_points[1].X - _points[0].X));
            float constant = _points[1].Y - (gradient * _points[1].X);
            if (point.Y>((gradient*point.X)+constant))
            {
                return false;
            }
            //north-northwest line check
            gradient = ((float)(_points[1].Y - _points[2].Y)) / ((float)(_points[1].X - _points[2].X));
            constant = _points[1].Y - (gradient * _points[1].X);
            if (point.Y > ((gradient * point.X) + constant))
            {
                return false;
            }
            //south-southeast line check
            gradient = ((float)(_points[4].Y - _points[5].Y)) / ((float)(_points[4].X - _points[5].X));
            constant = _points[4].Y - (gradient * _points[4].X);
            if (point.Y < ((gradient * point.X) + constant))
            {
                return false;
            }
            //south-southwest line check
            gradient = ((float)(_points[3].Y - _points[4].Y)) / ((float)(_points[3].X - _points[4].X));
            constant = _points[3].Y - (gradient * _points[3].X);
            if (point.Y < ((gradient * point.X) + constant))
            {
                return false;
            }
            return true;
        }

        public void PrintPoints()
        {
            for (int i = 0; i<6; i++)
            {
                MessageBox.Show("Point " + i + " has x = " + _points[i].X + " and y = " + _points[i].Y);
            }
        }

        public float DistanceFromCentre(Point point)
        {
            return (float)Math.Sqrt((point.X-_centre.X) * (point.X - _centre.X) + (point.Y - _centre.Y) * (point.Y - _centre.Y));
        }

        
    }
}
